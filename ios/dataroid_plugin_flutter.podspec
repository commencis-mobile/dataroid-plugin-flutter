#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint dataroid_plugin_flutter.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'dataroid_plugin_flutter'
  s.version          = '3.0.2'
  s.summary          = 'Dataroid Plugin'
  s.description      = <<-DESC
Dataroid flutter plugin project.
                       DESC
  s.homepage         = 'http://www.dataroid.com'
  s.license          = { :file => '../LICENSE' }
  s.author = { 'iOS Developers' => 'iosdevelopers@commencis.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'Connect-SDK-iOS', '3.9.3'
  s.platform = :ios, '11.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' =>'i386 arm64' }
  s.swift_version = '5.0'

  s.vendored_frameworks = 'AppConnect.framework'
end
