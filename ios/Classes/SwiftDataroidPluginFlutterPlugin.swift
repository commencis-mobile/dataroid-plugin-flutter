import Flutter
import UIKit
import AppConnect

public class SwiftDataroidPluginFlutterPlugin: NSObject, FlutterPlugin {

    public static var shared: SwiftDataroidPluginFlutterPlugin?

    private let channel: FlutterMethodChannel
    private var client: AppConnectClient?
    private let inAppDeeplinkHandler: InAppMessageDeeplinkHandler
    private let inAppSubscriber: InAppMessageSubscriber

    private var customEventAttributes: [String: Attributes] = [:]

    init(channel: FlutterMethodChannel) {
        self.channel = channel
        self.inAppDeeplinkHandler = InAppMessageDeeplinkHandler(channel: channel)
        self.inAppSubscriber = InAppMessageSubscriber(channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        DispatchQueue.main.async {
            let arguments = call.arguments as? [String: Any] ?? [:]

            guard let client = self.client else {
                result(FlutterError.with("AppConnect must be initialized first!"))
                return
            }

            switch call.method {

            case MethodName.collectCustomEvent:
                self.handleCollectCustomEvent(client: client, arguments: arguments, result: result)

            case MethodName.setUser:
                self.handleSetUser(client: client, arguments: arguments, result: result)

            case MethodName.updateUserAttributes:
                self.handleUpdateUser(client: client, arguments: arguments, result: result)

            case MethodName.clearUser:
                self.handleClearUser(client: client, result: result)

            case MethodName.collectAPMHTTPRecord:
                self.handleCollectAPMHTTPRecord(client: client, arguments: arguments, result: result)

            case MethodName.collectAPMNetworkErrorRecord:
                self.handleCollectAPMNetworkRecord(client: client, arguments: arguments, result: result)

            case MethodName.addToCart:
                self.handleAddToCart(client: client, arguments: arguments, result: result)

            case MethodName.addToWishList:
                self.handleAddToWishList(client: client, arguments: arguments, result: result)

            case MethodName.clearCart:
                self.handleClearCart(client: client, arguments: arguments, result: result)

            case MethodName.purchase:
                self.handlePurchase(client: client, arguments: arguments, result: result)

            case MethodName.removeFromCart:
                self.handleRemoveFromCart(client: client, arguments: arguments, result: result)

            case MethodName.search:
                self.handleSearch(client: client, arguments: arguments, result: result)

            case MethodName.startCheckout:
                self.handleStartCheckout(client: client, arguments: arguments, result: result)

            case MethodName.removeFromWishList:
                self.handleRemoveFromWishList(client: client, arguments: arguments, result: result)

            case MethodName.viewCategory:
                self.handleViewCategory(client: client, arguments: arguments, result: result)

            case MethodName.viewProduct:
                self.handleViewProduct(client: client, arguments: arguments, result: result)

            case MethodName.updateConfig:
                self.handleUpdateConfig(client: client, arguments: arguments, result: result)

            case MethodName.enableGeofencing:
                self.handleEnableGeofencing(client: client, result: result)

            case MethodName.disableGeofencing:
                self.handleDisableGeofencing(client: client, result: result)

            case MethodName.updateLanguage:
                self.handleUpdateLanguage(client: client, arguments: arguments, result: result)

            case MethodName.collectDeeplink:
                self.handleCollectDeeplink(client: client, arguments: arguments, result: result)

            case MethodName.startTracking:
                self.handleStartTracking(client: client, arguments: arguments, result: result)

            case MethodName.stopTracking:
                self.handleStopTracking(client: client, arguments: arguments, result: result)

            case MethodName.fetchMessages:
                self.handleInboxFetchMessages(client: client, arguments: arguments, result: result)

            case MethodName.deleteMessages:
                self.handleInboxDeleteMessages(client: client, arguments: arguments, result: result)

            case MethodName.readMessages:
                self.handleInboxReadMessages(client: client, arguments: arguments, result: result)

            case MethodName.setSuperAttribute:
                self.handleSetSuperAttribute(client: client, arguments: arguments, result: result)

            case MethodName.clearSuperAttribute:
                self.handleClearSuperAttribute(client: client, arguments: arguments, result: result)

            case MethodName.deviceInfo:
                self.handleGetDeviceInfo(client: client, result: result)

            case MethodName.collectButtonClickEvent:
                self.handleCollectButtonClickEvent(client: client, arguments: arguments, result: result)
                
            case MethodName.collectTextChangeEvent:
                self.handleCollectTextChangeEvent(client: client, arguments: arguments, result: result)
                
            case MethodName.collectToggleChangeEvent:
                self.handleCollectToggleChangeEvent(client: client, arguments: arguments, result: result)
                
            case MethodName.collectTouchEvent:
                self.handleCollectTouchEvent(client: client, arguments: arguments, result: result)

            case MethodName.collectDoubleTapEvent:
                self.handleCollectDoubleTapEvent(client: client, arguments: arguments, result: result)

            case MethodName.collectLongPressEvent:
                self.handleCollectLongPressEvent(client: client, arguments: arguments, result: result)

            case MethodName.collectSwipeEvent:
                self.handleCollectSwipeEvent(client: client, arguments: arguments, result: result)
                
            default:
                result(false);
                break
            }
        }
    }

    // MARK: - Component Interaction

    func handleCollectButtonClickEvent(client: AppConnectClient,
                                       arguments: [String: Any],
                                       result: @escaping FlutterResult) {
        var coordinates : UIViewPixelCoordinate?

        guard let className = arguments[ArgumentName.className] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        let label = arguments[ArgumentName.label] as? String
        let accessibilityLabel = arguments[ArgumentName.accessibilityLabel] as? String
        let componentId = arguments[ArgumentName.componentId] as? String

        if let coordinatesList = arguments[ArgumentName.coordinates] as? [String: AnyHashable] {
            guard let left = coordinatesList[ArgumentName.left] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let top = coordinatesList[ArgumentName.top] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let right = coordinatesList[ArgumentName.right] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let bottom = coordinatesList[ArgumentName.bottom] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            coordinates = UIViewPixelCoordinate(frame: convertCoordinatesToCGRect(left: left,
                                                                                  top: top,
                                                                                  right: right,
                                                                                  bottom: bottom))
        }

        var viewLabel : String?
        var viewClass : String?

        if let screenTrackingAttributes = arguments[ArgumentName.screenTrackingAttributes] as? [String: AnyHashable] {
            guard let viewLabelInput = screenTrackingAttributes[ArgumentName.label] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let viewClassInput = screenTrackingAttributes[ArgumentName.viewClass] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            viewLabel = viewLabelInput
            viewClass = viewClassInput
        }

        let buttonAttributes = ButtonTrackingAttributes(
            className: className,
            label: label,
            accessibilityLabel: accessibilityLabel,
            componentId: componentId,
            coordinates: coordinates,
            viewLabel: viewLabel,
            viewClass: viewClass
        )
        //Add domain for multiple instance
        buttonAttributes.collect()
        result(true)
    }

    func handleCollectTextChangeEvent(client: AppConnectClient,
                                      arguments: [String: Any],
                                      result: @escaping FlutterResult) {
        var coordinates : UIViewPixelCoordinate?
        
        guard let className = arguments[ArgumentName.className] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let value = arguments[ArgumentName.value] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        let placeholder = arguments[ArgumentName.placeholder] as? String
        let accessibilityLabel = arguments[ArgumentName.accessibilityLabel] as? String
        let componentId = arguments[ArgumentName.componentId] as? String

        if let coordinatesList = arguments[ArgumentName.coordinates] as? [String: AnyHashable] {
            guard let left = coordinatesList[ArgumentName.left] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let top = coordinatesList[ArgumentName.top] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let right = coordinatesList[ArgumentName.right] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let bottom = coordinatesList[ArgumentName.bottom] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            coordinates = UIViewPixelCoordinate(frame: convertCoordinatesToCGRect(left: left,
                                                                                  top: top,
                                                                                  right: right,
                                                                                  bottom: bottom))
        }

        var viewLabel: String?
        var viewClass: String?

        if let screenTrackingAttributes = arguments[ArgumentName.screenTrackingAttributes] as? [String: AnyHashable] {
            guard let viewLabelInput = screenTrackingAttributes[ArgumentName.label] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let viewClassInput = screenTrackingAttributes[ArgumentName.viewClass] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            viewLabel = viewLabelInput
            viewClass = viewClassInput
        }

        let textFieldAttributes = TextFieldTrackingAttributes(
            className: className,
            value: value,
            placeholder: placeholder,
            accessibilityLabel: accessibilityLabel,
            componentId: componentId,
            coordinates: coordinates,
            viewLabel: viewLabel,
            viewClass: viewClass
        )
        //Add domain for multiple instance
        textFieldAttributes.collect()
        result(true)
    }
    
    func handleCollectToggleChangeEvent(client: AppConnectClient,
                                        arguments: [String: Any],
                                        result: @escaping FlutterResult) {
        var coordinates : UIViewPixelCoordinate?
        
        guard let className = arguments[ArgumentName.className] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let isChecked = arguments[ArgumentName.value] as? Bool else {
            result(FlutterError.insufficientArguments)
            return
        }

        let accessibilityLabel = arguments[ArgumentName.accessibilityLabel] as? String
        let componentId = arguments[ArgumentName.componentId] as? String

        if let coordinatesList = arguments[ArgumentName.coordinates] as? [String: AnyHashable] {
            guard let left = coordinatesList[ArgumentName.left] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let top = coordinatesList[ArgumentName.top] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let right = coordinatesList[ArgumentName.right] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let bottom = coordinatesList[ArgumentName.bottom] as? Int else {
                result(FlutterError.insufficientArguments)
                return
            }
            coordinates = UIViewPixelCoordinate(frame: convertCoordinatesToCGRect(left: left,
                                                                                  top: top,
                                                                                  right: right,
                                                                                  bottom: bottom))
        }

        var viewLabel : String?
        var viewClass : String?

        if let screenTrackingAttributes = arguments[ArgumentName.screenTrackingAttributes] as? [String: AnyHashable] {
            guard let viewLabelInput = screenTrackingAttributes[ArgumentName.label] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let viewClassInput = screenTrackingAttributes[ArgumentName.viewClass] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            viewLabel = viewLabelInput
            viewClass = viewClassInput
        }

        let switchButtonAttributes = SwitchButtonTrackingAttributes(
            className: className,
            isChecked: isChecked,
            accessibilityLabel: accessibilityLabel,
            componentId: componentId,
            coordinates: coordinates,
            viewLabel: viewLabel,
            viewClass: viewClass
        )
        //Add domain for multiple instance
        switchButtonAttributes.collect()
        result(true)
    }
    
    func convertCoordinatesToCGRect(left: Int, top: Int, right: Int, bottom: Int) -> CGRect {
        return CGRect(x: left, y: bottom, width: right - left, height: top - bottom)
    }
    
    // MARK: - Screen Tracking

    func handleCollectTouchEvent(client: AppConnectClient,
                                 arguments: [String: Any],
                                 result: @escaping FlutterResult) {
        var viewLabel : String?
        var viewClass : String?

        if let screenTrackingAttributes = arguments[ArgumentName.screenTrackingAttributes] as? [String: AnyHashable] {
            guard let viewLabelInput = screenTrackingAttributes[ArgumentName.label] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let viewClassInput = screenTrackingAttributes[ArgumentName.viewClass] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            viewLabel = viewLabelInput
            viewClass = viewClassInput
        }
        
        guard let touchPointData = arguments[ArgumentName.touchPoint] as? [String: Any] else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let touchPointX = touchPointData[ArgumentName.x] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let touchPointY = touchPointData[ArgumentName.y] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        
        let touchPoint = TouchPoint(point: CGPoint(x: touchPointX, y: touchPointY))
        let tapAttributes = TapTrackingAttributes(
            touchPoint: touchPoint,
            viewLabel: viewLabel,
            viewClass: viewClass
        )
        //Add domain for multiple instance
        tapAttributes.collect()
        result(true)
    }
    
    func handleCollectDoubleTapEvent(client: AppConnectClient,
                                     arguments: [String: Any],
                                     result: @escaping FlutterResult) {
        var viewLabel : String?
        var viewClass : String?

        if let screenTrackingAttributes = arguments[ArgumentName.screenTrackingAttributes] as? [String: AnyHashable] {
            guard let viewLabelInput = screenTrackingAttributes[ArgumentName.label] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let viewClassInput = screenTrackingAttributes[ArgumentName.viewClass] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            viewLabel = viewLabelInput
            viewClass = viewClassInput
        }
        
        guard let touchPointData = arguments[ArgumentName.touchPoint] as? [String: Any] else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let touchPointX = touchPointData[ArgumentName.x] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let touchPointY = touchPointData[ArgumentName.y] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        
        let touchPoint = TouchPoint(point: CGPoint(x: touchPointX, y: touchPointY))
        let doubleTapAttributes = DoubleTapTrackingAttributes(
            touchPoint: touchPoint,
            viewLabel: viewLabel,
            viewClass: viewClass
        )
        //Add domain for multiple instance
        doubleTapAttributes.collect()
        result(true)
    }

    func handleCollectLongPressEvent(client: AppConnectClient,
                                     arguments: [String: Any],
                                     result: @escaping FlutterResult) {
        var viewLabel : String?
        var viewClass : String?

        if let screenTrackingAttributes = arguments[ArgumentName.screenTrackingAttributes] as? [String: AnyHashable] {
            guard let viewLabelInput = screenTrackingAttributes[ArgumentName.label] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let viewClassInput = screenTrackingAttributes[ArgumentName.viewClass] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            viewLabel = viewLabelInput
            viewClass = viewClassInput
        }
        
        guard let touchPointData = arguments[ArgumentName.touchPoint] as? [String: Any] else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let touchPointX = touchPointData[ArgumentName.x] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let touchPointY = touchPointData[ArgumentName.y] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        
        let touchPoint = TouchPoint(point: CGPoint(x: touchPointX, y: touchPointY))
        let longPressAttributes = LongPressTrackingAttributes(
            touchPoint: touchPoint,
            viewLabel: viewLabel,
            viewClass: viewClass
        )
        //Add domain for multiple instance
        longPressAttributes.collect()
        result(true)
    }
    
    func handleCollectSwipeEvent(client: AppConnectClient,
                                 arguments: [String: Any],
                                 result: @escaping FlutterResult) {
        var viewLabel : String?
        var viewClass : String?

        if let screenTrackingAttributes = arguments[ArgumentName.screenTrackingAttributes] as? [String: AnyHashable] {
            guard let viewLabelInput = screenTrackingAttributes[ArgumentName.label] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            guard let viewClassInput = screenTrackingAttributes[ArgumentName.viewClass] as? String else {
                result(FlutterError.insufficientArguments)
                return
            }
            viewLabel = viewLabelInput
            viewClass = viewClassInput
        }
        
        guard let swipePoints = arguments[ArgumentName.swipePoints] as? [String: Any] else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let startPointData = swipePoints[ArgumentName.start] as? [String: Any] else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let endPointData = swipePoints[ArgumentName.end] as? [String: Any] else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let startPointX = startPointData[ArgumentName.x] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let startPointY = startPointData[ArgumentName.y] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let endPointX = endPointData[ArgumentName.x] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let endPointY = endPointData[ArgumentName.y] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }
        
        let startPoint = CGPoint(x: startPointX, y: startPointY)
        let endPoint = CGPoint(x: endPointX, y: endPointY)
        let swipeAttributes = SwipeTrackingAttributes(
            startPoint: startPoint,
            endPoint: endPoint,
            viewLabel: viewLabel,
            viewClass: viewClass
        )
        //Add domain for multiple instance
        swipeAttributes.collect()
        result(true)
    }

    // MARK: - Custom events

    func handleCollectCustomEvent(client: AppConnectClient,
                                  arguments: [String: Any],
                                  result: @escaping FlutterResult) {
        guard let eventName = arguments[ArgumentName.name] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }
        //Add domain for multiple instance
        let attributes = Attributes(name: eventName, domain: nil)
        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    attributes.addNumber(NSNumber(value: value), forKey: key)
                }
                else if let value = _value as? Float {
                    attributes.addNumber(NSNumber(value: value), forKey: key)
                }
                else if let value = _value as? Double {
                    attributes.addNumber(NSNumber(value: value), forKey: key)
                }
                else if let value = _value as? Bool {
                    attributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    attributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                attributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                attributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                attributes.addStringArray(value, forKey: key)
            }
        }

        attributes.collect()
        result(true)
    }

    // MARK: - User

    func handleSetUser(client: AppConnectClient,
                       arguments: [String: Any],
                       result: @escaping FlutterResult) {
        guard let customerId = arguments[ArgumentName.customerId] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        let user = AppConnectUser(customerId: customerId)
        user.email = arguments[ArgumentName.email] as? String
        user.phone = arguments[ArgumentName.phone] as? String
        user.nationalId = arguments[ArgumentName.nationalId] as? String
        user.firstName = arguments[ArgumentName.firstName] as? String
        user.lastName = arguments[ArgumentName.lastName] as? String

        if let timeInterval = arguments[ArgumentName.dateOfBirth] as? Double {
            user.dateOfBirth = Date(timeIntervalSince1970: timeInterval / 1000)
        }

        if let genderIndex = arguments[ArgumentName.genderIndex] as? Int,
           let gender = Gender(rawValue: genderIndex) {
            user.gender = gender
        }

        let userAttributes = UserAttributes()
        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Bool {
                    userAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? Int {
                    userAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    userAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    userAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? String {
                    userAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                userAttributes.addDate(date, forKey: key)
            }
        }

        user.attributes = userAttributes
        client.setUser(user)
        result(true)
    }

    func handleUpdateUser(client: AppConnectClient,
                          arguments: [String: Any],
                          result: @escaping FlutterResult) {
        let userAttributes = UserAttributes()
        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Bool {
                    userAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? Int {
                    userAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Int {
                    userAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    userAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    userAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? String {
                    userAttributes.addString(value, forKey: key)
                }
            }
        }
        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                userAttributes.addDate(date, forKey: key)
            }
        }

        client.updateUser(attributes: userAttributes)
        result(true)
    }

    func handleClearUser(client: AppConnectClient, result: @escaping FlutterResult) {
        client.clearUser()
        result(true)
    }

    // MARK: - APM

    func handleCollectAPMHTTPRecord(client: AppConnectClient,
                                    arguments: [String: Any],
                                    result: @escaping FlutterResult) {
        guard let url = arguments[ArgumentName.url] as? String,
              let statusCode = arguments[ArgumentName.statusCode] as? Int,
              let duration = arguments[ArgumentName.duration] as? Double,
              let success = arguments[ArgumentName.success] as? Bool else {
            result(FlutterError.insufficientArguments)
            return
        }

        guard let httpMethod = SwiftDataroidPluginFlutterPlugin.parseHTTPMethod(value: arguments[ArgumentName.method]) else {
            result(FlutterError.insufficientArguments)
            return
        }

        // For multiple instances add domain
        let record = APMHTTPRecord(
            url: url,
            method: httpMethod,
            statusCode: statusCode,
            duration: duration,
            success: success,
            domain: nil
        )
        if let value = arguments[ArgumentName.requestSize] as? Double {
            record.requestSize = value
        }
        if let value = arguments[ArgumentName.responseSize] as? Double {
            record.responseSize = value
        }

        record.errorType = arguments[ArgumentName.errorType] as? String
        record.errorCode = arguments[ArgumentName.errorCode] as? String
        record.errorMessage = arguments[ArgumentName.errorMessage] as? String

        let attributes = APMAttributes()

        if let attributesList = arguments[ArgumentName.customAttributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    attributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    attributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    attributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    attributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    attributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                attributes.addDate(date, forKey: key)
            }
        }

        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                attributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                attributes.addStringArray(value, forKey: key)
            }
        }

        record.attributes = attributes
        client.collectAPMHTTPRecord(record)
        result(true)
    }

    func handleCollectAPMNetworkRecord(client: AppConnectClient,
                                       arguments: [String: Any],
                                       result: @escaping FlutterResult) {
        guard let url = arguments[ArgumentName.url] as? String,
              let duration = arguments[ArgumentName.duration] as? Double,
              let exception = arguments[ArgumentName.exception] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        guard let httpMethod = SwiftDataroidPluginFlutterPlugin.parseHTTPMethod(value: arguments[ArgumentName.method]) else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let typeIndex = arguments[ArgumentName.type] as? Int else {
            result(FlutterError.insufficientArguments)
            return
        }

        let record = APMNetworkRecord(
            url: url,
            method: httpMethod,
            duration: duration,
            type: APMNetworkRecord.ErrorType(rawValue: typeIndex),
            exception: exception
        )

        record.message = arguments[ArgumentName.message] as? String

        let attributes = APMAttributes()

        if let attributesList = arguments[ArgumentName.customAttributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    attributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    attributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    attributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    attributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    attributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                attributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                attributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                attributes.addStringArray(value, forKey: key)
            }
        }

        record.attributes = attributes
        client.collectAPMNetworkRecord(record)
        result(true)
    }

    // MARK: - Commerce

    func handleAddToCart(client: AppConnectClient,
                         arguments: [String: Any],
                         result: @escaping FlutterResult) {
        guard let product = SwiftDataroidPluginFlutterPlugin.parseProduct(
            arguments: arguments[ArgumentName.product],
            result: result
        ) else {
            result(FlutterError.insufficientArguments)
            return
        }
        //Add domain for multiple instance
        let addToCartAttributes = AddToCardEventAttributes(product: product, domain: nil)
        if let value = arguments[ArgumentName.value] as? Int {
            addToCartAttributes.addValue(Decimal(value))
        }
        if let totalValue = arguments[ArgumentName.totalCartValue] as? Int {
            addToCartAttributes.addTotalCartValue(Decimal(totalValue))
        }
        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    addToCartAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    addToCartAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    addToCartAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    addToCartAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    addToCartAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                addToCartAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                addToCartAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                addToCartAttributes.addStringArray(value, forKey: key)
            }
        }

        addToCartAttributes.collect()
        result(true)
    }

    func handleAddToWishList(client: AppConnectClient,
                             arguments: [String: Any],
                             result: @escaping FlutterResult) {
        guard let product: Product = SwiftDataroidPluginFlutterPlugin.parseProduct(
            arguments: arguments[ArgumentName.product],
            result: result
        ) else {
            result(FlutterError.insufficientArguments)
            return
        }
        //Add domain for multiple instance
        let addToWishListEventAttributes = AddToWishListEventAttributes(product: product, domain: nil)

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    addToWishListEventAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    addToWishListEventAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    addToWishListEventAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    addToWishListEventAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    addToWishListEventAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                addToWishListEventAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                addToWishListEventAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                addToWishListEventAttributes.addStringArray(value, forKey: key)
            }
        }

        addToWishListEventAttributes.collect()
        result(true)
    }

    func handleClearCart(client: AppConnectClient,
                         arguments: [String: Any],
                         result: @escaping FlutterResult) {
        //Add domain for multiple instance
        let clearCartEventAttributes = ClearCartEventAttributes(domain: nil)
        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    clearCartEventAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    clearCartEventAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    clearCartEventAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    clearCartEventAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    clearCartEventAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                clearCartEventAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                clearCartEventAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                clearCartEventAttributes.addStringArray(value, forKey: key)
            }
        }

        clearCartEventAttributes.collect()
        result(true)
    }

    func handlePurchase(client: AppConnectClient,
                        arguments: [String: Any],
                        result: @escaping FlutterResult) {
        guard let productsJSONArray = arguments[ArgumentName.products] as? [Any] else {
            result(FlutterError.insufficientArguments)
            return
        }
        let products = productsJSONArray.compactMap { SwiftDataroidPluginFlutterPlugin.parseProduct(arguments: $0, result: result) }

        guard let currency = arguments[ArgumentName.currency] as? String,
              let value = arguments[ArgumentName.value] as? Double,
              let success = arguments[ArgumentName.success] as? Bool else {
            result(FlutterError.insufficientArguments)
            return
        }

        //Add domain for multiple instance
        let purchaseEventAttributes = PurchaseEventAttributes(
            currency: currency,
            value: Decimal(value),
            products: products,
            success: success,
            domain: nil
        )

        if let tax = arguments[ArgumentName.tax] as? Double {
            purchaseEventAttributes.addTax(Decimal(tax))
        }

        if let ship = arguments[ArgumentName.ship] as? Double {
            purchaseEventAttributes.addShip(Decimal(ship))
        }

        if let discount = arguments[ArgumentName.discount] as? Double {
            purchaseEventAttributes.addDiscount(Decimal(discount))
        }

        if let coupon = arguments[ArgumentName.coupon] as? String {
            purchaseEventAttributes.addCoupon(coupon)
        }

        if let trxId = arguments[ArgumentName.trxId] as? String {
            purchaseEventAttributes.addTrxId(trxId)
        }

        if let paymentMethod = arguments[ArgumentName.paymentMethod] as? String {
            purchaseEventAttributes.addPaymentMethod(paymentMethod)
        }

        if let quantity = arguments[ArgumentName.quantity] as? Int {
            purchaseEventAttributes.addQuantity(quantity)
        }

        if let errorCode = arguments[ArgumentName.errorCode] as? String {
            purchaseEventAttributes.addErrorCode(errorCode)
        }

        if let errorMessage = arguments[ArgumentName.errorMessage] as? String {
            purchaseEventAttributes.addErrorMessage(errorMessage)
        }

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    purchaseEventAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    purchaseEventAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    purchaseEventAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    purchaseEventAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    purchaseEventAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                purchaseEventAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                purchaseEventAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                purchaseEventAttributes.addStringArray(value, forKey: key)
            }
        }
        
        purchaseEventAttributes.collect()
        result(true)
    }

    func handleRemoveFromCart(client: AppConnectClient,
                              arguments: [String: Any],
                              result: @escaping FlutterResult) {
        guard let product = SwiftDataroidPluginFlutterPlugin.parseProduct(
            arguments: arguments[ArgumentName.product],
            result: result
        ) else {
            result(FlutterError.insufficientArguments)
            return
        }
        //Add domain for multiple instance
        let removeFromCartEventAttributes = RemoveFromCartEventAttributes(
            product: product,
            domain: nil
        )

        if let value = arguments[ArgumentName.value] as? Int {
            removeFromCartEventAttributes.addValue(Decimal(value))
        }
        if let totalValue = arguments[ArgumentName.totalCartValue] as? Int {
            removeFromCartEventAttributes.addTotalCartValue(Decimal(totalValue))
        }
        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    removeFromCartEventAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    removeFromCartEventAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    removeFromCartEventAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    removeFromCartEventAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    removeFromCartEventAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String : Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                removeFromCartEventAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String : [Int]] {
            for(key, value) in intListAttributes {
                removeFromCartEventAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String : [String]] {
            for(key, value) in stringListAttributes {
                removeFromCartEventAttributes.addStringArray(value, forKey: key)
            }
        }

        removeFromCartEventAttributes.collect()
        result(true)
    }

    func handleSearch(client: AppConnectClient,
                      arguments: [String: Any],
                      result: @escaping FlutterResult) {
        guard let query = arguments[ArgumentName.query] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        //Add domain for multiple instance
        let searchAttributes = SearchEventAttributes(query: query, domain: nil)

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    searchAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    searchAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    searchAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    searchAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    searchAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                searchAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                searchAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                searchAttributes.addStringArray(value, forKey: key)
            }
        }

        searchAttributes.collect()
        result(true)
    }

    func handleStartCheckout(client: AppConnectClient,
                             arguments: [String: Any],
                             result: @escaping FlutterResult) {
        guard let value = arguments[ArgumentName.value] as? Int,
              let currency = arguments[ArgumentName.currency] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        //Add domain for multiple instance
        let startCheckoutEventAttributes = StartCheckoutEventAttributes(
            value: Decimal(value),
            currency: currency,
            domain: nil
        )

        if let quantity = arguments[ArgumentName.quantity] as? Int {
            startCheckoutEventAttributes.addQuantity(quantity)
        }

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable]{
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    startCheckoutEventAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    startCheckoutEventAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    startCheckoutEventAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    startCheckoutEventAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    startCheckoutEventAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                startCheckoutEventAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                startCheckoutEventAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                startCheckoutEventAttributes.addStringArray(value, forKey: key)
            }
        }
        
        startCheckoutEventAttributes.collect()
        result(true)
    }

    func handleRemoveFromWishList(client: AppConnectClient,
                                  arguments: [String: Any],
                                  result: @escaping FlutterResult) {
        guard let product = SwiftDataroidPluginFlutterPlugin.parseProduct(
            arguments: arguments[ArgumentName.product],
            result: result
        ) else {
            result(FlutterError.insufficientArguments)
            return
        }

        //Add domain for multiple instance
        let removeFromWishListAttributes = RemoveFromWishListEventAttributes(
            product: product,
            domain: nil
        )

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    removeFromWishListAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    removeFromWishListAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    removeFromWishListAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    removeFromWishListAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    removeFromWishListAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                removeFromWishListAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                removeFromWishListAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                removeFromWishListAttributes.addStringArray(value, forKey: key)
            }
        }
        
        removeFromWishListAttributes.collect()
        result(true)
    }

    func handleViewCategory(client: AppConnectClient,
                            arguments: [String: Any],
                            result: @escaping FlutterResult) {
        guard let category = arguments[ArgumentName.category] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        //Add domain for multiple instance
        let viewCategoryEventAttributes = ViewCategoryEventAttributes(
            category: category,
            domain: nil
        )

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    viewCategoryEventAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    viewCategoryEventAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    viewCategoryEventAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    viewCategoryEventAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    viewCategoryEventAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                viewCategoryEventAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                viewCategoryEventAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                viewCategoryEventAttributes.addStringArray(value, forKey: key)
            }
        }

        viewCategoryEventAttributes.collect()
        result(true)
    }

    func handleViewProduct(client: AppConnectClient,
                           arguments: [String: Any],
                           result: @escaping FlutterResult) {
        guard let product = SwiftDataroidPluginFlutterPlugin.parseProduct(
            arguments: arguments[ArgumentName.product],
            result: result
        ) else {
            result(FlutterError.insufficientArguments)
            return
        }

        //Add domain for multiple instance
        let viewProductEventAttributes = ViewProductEventAttributes(
            product: product,
            domain: nil
        )

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    viewProductEventAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    viewProductEventAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    viewProductEventAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    viewProductEventAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    viewProductEventAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                viewProductEventAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                viewProductEventAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                viewProductEventAttributes.addStringArray(value, forKey: key)
            }
        }

        viewProductEventAttributes.collect()
        result(true)
    }

    // MARK: - Extras

    func handleEnableGeofencing(client: AppConnectClient, result: @escaping FlutterResult) {
        client.enableGeofencing()
        result(true)
    }

    func handleDisableGeofencing(client: AppConnectClient, result: @escaping FlutterResult) {
        client.disableGeofencing()
        result(true)
    }

    func handleUpdateLanguage(client: AppConnectClient,
                              arguments: [String: Any],
                              result: @escaping FlutterResult) {
        guard let languageCode = arguments[ArgumentName.languageCode] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }
        client.updateLanguage(languageCode: languageCode)
        result(true)
    }

    // MARK: - Screen Tracking

    func handleStartTracking(client: AppConnectClient,
                             arguments: [String: Any],
                             result: @escaping FlutterResult) {
        guard let viewClass = arguments[ArgumentName.viewClass] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let label = arguments[ArgumentName.label] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        let extras = ViewTrackingExtras()
        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    extras.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    extras.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    extras.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    extras.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    extras.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                extras.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                extras.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                extras.addStringArray(value, forKey: key)
            }
        }

        client.screenTracker.startTracking(viewClass: viewClass, name: label, extras: extras)
        result(true)
    }

    func handleStopTracking(client: AppConnectClient,
                            arguments: [String: Any],
                            result: @escaping FlutterResult) {
        guard let viewClass = arguments[ArgumentName.viewClass] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }
        guard let label = arguments[ArgumentName.label] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        //TODO: Add view argument
        client.screenTracker.stopTracking(viewClass: viewClass, name: label)

        result(true)
    }

    // MARK: - Deeplink

    func handleCollectDeeplink(client: AppConnectClient,
                               arguments: [String: Any],
                               result: @escaping FlutterResult) {
        guard let urlString = arguments[ArgumentName.url] as? String,
              let url = URL(string: urlString) else {
            result(FlutterError.insufficientArguments)
            return
        }
        // Not used in iOS SDK should be removed when it's removed from there.
        let options: [UIApplication.OpenURLOptionsKey: Any] = [:]

        //Add domain for multiple instance
        let deeplinkAttributes = DeeplinkAttributes(
            url: url,
            options: options,
            domain: nil
        )

        if let attributesList = arguments[ArgumentName.attributes] as? [String: AnyHashable] {
            for(key, _value) in attributesList {
                if let value = _value as? Int {
                    deeplinkAttributes.addInt(value, forKey: key)
                }
                else if let value = _value as? Float {
                    deeplinkAttributes.addFloat(value, forKey: key)
                }
                else if let value = _value as? Double {
                    deeplinkAttributes.addDouble(value, forKey: key)
                }
                else if let value = _value as? Bool {
                    deeplinkAttributes.addBool(value, forKey: key)
                }
                else if let value = _value as? String {
                    deeplinkAttributes.addString(value, forKey: key)
                }
            }
        }

        if let dateAttributes = arguments[ArgumentName.dateAttributes] as? [String: Double] {
            for(key, value) in dateAttributes {
                let date = Date(timeIntervalSince1970: value / 1000)
                deeplinkAttributes.addDate(date, forKey: key)
            }
        }
        if let intListAttributes = arguments[ArgumentName.intListAttributes] as? [String: [Int]] {
            for(key, value) in intListAttributes {
                deeplinkAttributes.addIntArray(value, forKey: key)
            }
        }
        if let stringListAttributes = arguments[ArgumentName.stringListAttributes] as? [String: [String]] {
            for(key, value) in stringListAttributes {
                deeplinkAttributes.addStringArray(value, forKey: key)
            }
        }

        deeplinkAttributes.collect()
        result(true)
    }

    // MARK: - App Inbox

    func handleInboxFetchMessages(client: AppConnectClient,
                                  arguments: [String: Any],
                                  result: @escaping FlutterResult) {
        var query: AppInboxQuery?
        if let queryJSON = arguments[ArgumentName.query] as? [String: Any] {
            query = AppInboxQuery()
            if let type = queryJSON[ArgumentName.messageType] as? Int,
               let messageType = InboxMessageType(rawValue: type) {
                query = query?.messageType(messageType)
            }
            if let status = queryJSON[ArgumentName.messageStatus] as? Int,
               let messageStatus = InboxMessageStatus(rawValue: status) {
                query = query?.status(messageStatus)
            }
            if let isAnonymous = queryJSON[ArgumentName.isAnonymous] as? Bool {
                query = query?.anonymous(isAnonymous)
            }
            if let fromInterval = queryJSON[ArgumentName.from] as? TimeInterval {
                let from = Date(timeIntervalSince1970: fromInterval / 1000)
                query = query?.from(from)
            }
            if let toInterval = queryJSON[ArgumentName.to] as? TimeInterval {
                let to = Date(timeIntervalSince1970: toInterval / 1000)
                query = query?.to(to)
            }
        }

        if let inboxMessages = client.inbox?.fetchMessages(query: query) {
            let messageList: [[String: Any]] = inboxMessages.map {
                var m: [String: Any] = [ArgumentName.id: "\($0.id ?? -1)",
                                        ArgumentName.messageType: $0.type.rawValue,
                                        ArgumentName.messageStatus: $0.status.rawValue]
                if let date = $0.receivedDate {
                    m[ArgumentName.receivedDate] = date.timeIntervalSince1970 * 1000
                }
                if let date = $0.expirationDate {
                    m[ArgumentName.expirationDate] = date.timeIntervalSince1970 * 1000
                }
                if let customerId = $0.customerId {
                    m[ArgumentName.customerId] = customerId
                }
                if let payload = $0.payload {
                    m[ArgumentName.payload] = payload
                }
                return m;
            }
            result(messageList);
        } else {
            result([])
        }
    }

    func handleInboxDeleteMessages(client: AppConnectClient,
                                   arguments: [String: Any],
                                   result: @escaping FlutterResult) {
        let messageIDList = arguments[ArgumentName.messageIDList] as? [String] ?? []
        let ret = client.inbox?.deleteMessages(ids: messageIDList.compactMap { Int64($0) })
        result(ret ?? false)
    }

    func handleInboxReadMessages(client: AppConnectClient,
                                 arguments: [String: Any],
                                 result: @escaping FlutterResult) {
        let messageIDList = arguments[ArgumentName.messageIDList] as? [String] ?? []
        let ret = client.inbox?.readMessages(ids: messageIDList.compactMap { Int64($0) })
        result(ret ?? false)
    }

    // MARK: - Super Attribute
    func handleSetSuperAttribute(client: AppConnectClient,
                                 arguments: [String: Any],
                                 result: @escaping FlutterResult) {
        guard let key = arguments[ArgumentName.key] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }

        if let _value = arguments[ArgumentName.attributes] as? AnyHashable {
            if let value = _value as? String {
                client.setSuperAttribute(key: key, value: value)
            }
            else if let value = _value as? Int {
                client.setSuperAttribute(key: key, intValue: value)
            }
            else if let value = _value as? Double {
                client.setSuperAttribute(key: key, doubleValue: value)
            }
            else if let value = _value as? Int64 {
                client.setSuperAttribute(key: key, int64Value: value)
            }
            else if let value = _value as? Float {
                client.setSuperAttribute(key: key, floatValue: value)
            }
            else if let value = _value as? Bool {
                client.setSuperAttribute(key: key, boolValue: value)
            }
        }

        if let dateValue = arguments[ArgumentName.dateAttributes] as? Double {
            let date = Date(timeIntervalSince1970: dateValue / 1000)
            client.setSuperAttribute(key: key, dateValue: date)
        }
        result(true)
    }

    func handleClearSuperAttribute(client: AppConnectClient,
                                   arguments: [String: Any],
                                   result: @escaping FlutterResult) {
        guard let key = arguments[ArgumentName.key] as? String else {
            result(FlutterError.insufficientArguments)
            return
        }
        client.clearSuperAttribute(key: key)
        result(true)
    }

    // MARK: - Device Info

    func handleGetDeviceInfo(client: AppConnectClient, result: @escaping FlutterResult) {
        let ret: [String: String] = [ArgumentName.deviceId: client.deviceId,
                                     ArgumentName.deviceProperties: client.devicePropertyString() ?? ""]
        result(ret)
    }

    // MARK: - Configuration

    func handleUpdateConfig(client: AppConnectClient,
                            arguments: [String: Any],
                            result: @escaping FlutterResult) {
        SwiftDataroidPluginFlutterPlugin.updateConfig(client.config, arguments: arguments)
        result(true)
    }
}

extension SwiftDataroidPluginFlutterPlugin {

    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(
            name: "dataroid_plugin_flutter",
            binaryMessenger: registrar.messenger()
        )
        shared = SwiftDataroidPluginFlutterPlugin(channel: channel)

        if let instance = shared {
            registrar.addMethodCallDelegate(instance, channel: channel)
            //registrar.addApplicationDelegate(instance)
        }
    }

    public static func initialize(config: AppConnectConfig) {
        guard let instance = shared else {
            print("[ERROR] Flutter plugin is not registered properly!")
            return
        }

        config.frameworkIdentifier = "FLUTTER"

        instance.client = AppConnectClient.initialize(config: config)

        instance.client?.alertTapListenerDelegate = instance.inAppDeeplinkHandler
        instance.client?.alertDeeplinkHandlerDelegate = instance.inAppDeeplinkHandler
        instance.client?.inAppMessageListenerDelegate = instance.inAppSubscriber
    }

    static func updateConfig(_ config: AppConnectConfig, arguments: [String: Any]) {
        if let eventCollectingDisabled = arguments[ArgumentName.eventCollectingDisabled] as? Bool {
            config.eventCollectingDisabled = eventCollectingDisabled
        }
        if let sessionDropDuration = arguments[ArgumentName.sessionDropDuration] as? Double {
            config.sessionDropDuration = sessionDropDuration
        }
        if let dictionary = arguments[ArgumentName.goal] as? [String: Any] {
            if let appGroupIdentifier = dictionary[ArgumentName.appGroupIdentifier] as? String {
                config.appGroupIdentifier = appGroupIdentifier
            }
        }
        if let dictionary = arguments[ArgumentName.snapshot] as? [String: Any] {
            if let recordingEnabled = dictionary[ArgumentName.recordingEnabled] as? Bool {
                config.snapshot.recordingEnabled = recordingEnabled
            }
            if let enabledBundleIDs = dictionary[ArgumentName.enabledBundleIDs] as? [String] {
                config.snapshot.enabledBundleIDs = enabledBundleIDs
            }
        }
        if let dictionary = arguments[ArgumentName.inAppMessaging] as? [String: Any] {
            if let inAppMessagingEnabled = dictionary[ArgumentName.inAppMessagingEnabled] as? Bool {
                config.inApp.inAppMessagingEnabled = inAppMessagingEnabled
            }
        }
        if let dictionary = arguments[ArgumentName.apm] as? [String: Any] {
            if let recordCollectionEnabled = dictionary[ArgumentName.recordCollectionEnabled] as? Bool {
                config.apm.recordCollectionEnabled = recordCollectionEnabled
            }
            if let recordStorageLimit = dictionary[ArgumentName.recordStorageLimit] as? Int {
                config.apm.recordStorageLimit = recordStorageLimit
            }
        }

        config.screenTracking.autoTrackingEnabled = false

        if let dictionary = arguments[ArgumentName.screenTracking] as? [String: Any] {
            if let enabled = dictionary[ArgumentName.enabled] as? Bool {
                config.screenTracking.enabled = enabled
            }
        }
        if let dictionary = arguments[ArgumentName.logger] as? [String: Any] {
            if let levelIndex = dictionary[ArgumentName.level] as? UInt,
               let level = LoggerLevel(rawValue: levelIndex) {
                config.logger.logLevel = level
            }
            if let writeToFile = dictionary[ArgumentName.writeToFile] as? Bool {
                config.logger.writeToFile = writeToFile
            }
        }
    }

    static func parseHTTPMethod(value: Any?) -> HTTPMethod? {
        guard let methodDescription = value as? String,
              !methodDescription.isEmpty else {
            return nil
        }

        var method: HTTPMethod?
        var i = 0
        repeat {
            method = HTTPMethod(rawValue: i)
            if (method?.description.lowercased() == methodDescription.lowercased()) {
                return method
            }
            i += 1
        } while (method != nil)

        return nil
    }

    static func parseProduct(arguments: Any?, result: @escaping FlutterResult) -> Product? {
        
        guard let productData = arguments as? [String: Any] else {
            result(FlutterError.insufficientArguments)
            return nil
        }
        
        guard let id = productData[ArgumentName.id] as? String,
              let name = productData[ArgumentName.name] as? String,
              let quantity = productData[ArgumentName.quantity] as? Int,
              let price = productData[ArgumentName.price] as? Double,
              let currency = productData[ArgumentName.currency] as? String else{
            result(FlutterError.insufficientArguments)
            return nil
        }

        let description = productData[ArgumentName.description] as? String
        let brand = productData[ArgumentName.brand] as? String
        let variant = productData[ArgumentName.variant] as? String
        let category = productData[ArgumentName.category] as? String
        
        let product = Product(
            id: id,
            name: name,
            quantity: quantity,
            price: Decimal(price),
            currency: currency
        )
        
        product?.productDescription = description
        product?.brand = brand
        product?.variant = variant
        product?.category = category
        
        return product
    }

    static func parseAttributes(arguments: [String: Any],
                                key: String = ArgumentName.attributes) -> [(String, Any)] {
        if let attributes = arguments[key] as? [[String: Any]] {
            let items: [(String, Any)] = attributes.compactMap {
                if let attributeKey = $0[ArgumentName.key] as? String,
                   let attributeValue = $0[ArgumentName.value],
                   !attributeKey.isEmpty {
                    return (attributeKey, attributeValue);
                }
                return nil
            }
            return items
        }
        return []
    }
}

// MARK: - Application LifeCycle

public extension SwiftDataroidPluginFlutterPlugin {

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        client?.listener.appDidFinishLaunching(withOptions: launchOptions)
    }

    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        client?.listener.appDidRegisterForRemoteNotifications(withDeviceToken: deviceToken)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        handleNotification(response.notification)
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        channel.invokeMethod(MethodName.shouldShowPushNotificationInForegroundiOS,
                             arguments: notification.request.content.userInfo) { (result) in
            let shouldShow = (result as? Bool) ?? false
            if shouldShow {
                completionHandler([.alert, .sound])
            } else {
                completionHandler([])
            }
        }
    }

    private func handleNotification(_ notification: UNNotification) {
        client?.listener.appDidReceiveRemoteNotification(notification.request.content.userInfo)

        func invoke(pushEvent: PushEvent, info: PushEventInfo, result: @escaping FlutterResult) {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(150)) {
                let timingInfoIndex: Int
                switch info {
                case .receivedWhenTerminatedAndOpenedTheApp:
                    timingInfoIndex = 0
                case .receivedInBackgroundAndOpenedTheApp:
                    timingInfoIndex = 1
                case .receivedInBackground:
                    timingInfoIndex = 1
                case .receivedInForeground:
                    timingInfoIndex = 3
                @unknown default:
                    timingInfoIndex = 3
                }

                var actionTypeIndex = 0
                if let type = pushEvent.actionType {
                    switch type {
                    case .none:
                        actionTypeIndex = 0
                    case .openApp:
                        actionTypeIndex = 1
                    case .gotoUrl:
                        actionTypeIndex = 2
                    case .gotoDeeplink:
                        actionTypeIndex = 3
                    @unknown default:
                        actionTypeIndex = 0
                    }
                }

                self.channel.invokeMethod(
                    MethodName.handlePushEventiOS,
                    arguments: [ArgumentName.pushEventTiming: timingInfoIndex,
                                ArgumentName.pushActionType: actionTypeIndex,
                                ArgumentName.pushTargetURL: pushEvent.targetURL?.absoluteString ?? "",
                                ArgumentName.pushAttrs: pushEvent.attributes ?? [:]],
                    result: result)
            }
        }

        guard let latest = client?.pushEventManager.latest else {
            return
        }

        // Wait for the system to register Flutter plugin, and user to assign its delegate.
        var retryCounter = 10
        func resultCallBack(_ result: Any?) {
            let isHandled = (result as? Bool) ?? false
            if (!isHandled && retryCounter >= 0) {
                invoke(pushEvent: latest.pushEvent, info: latest.info, result: resultCallBack)
            }
            retryCounter -= 1
        }

        invoke(pushEvent: latest.pushEvent, info: latest.info, result: resultCallBack)
    }
}

extension FlutterError {

    static func with(_ message: String) -> FlutterError {
        return FlutterError(code: "", message: message, details: nil)
    }

    static var insufficientArguments: FlutterError = .with("Insufficient arguments!")
}

final class InAppMessageDeeplinkHandler: InAppMessageAlertDeeplinkHandler, InAppMessageAlertTapListener {

    private let channel: FlutterMethodChannel

    init(channel: FlutterMethodChannel) {
        self.channel = channel
    }

    func shouldHandleDeeplink(deeplink: String?) {
        guard let deeplink = deeplink else {
            return
        }

        channel.invokeMethod(MethodName.handleDeeplink,
                             arguments: [ArgumentName.deeplink: deeplink])
    }

    func didTapAlert(button: InAppMessageButton, content: InAppMessageContent) {
        guard let inAppContent = try? JSONEncoder().encode(content),
              let inAppButton = try? JSONEncoder().encode(button) else {
            return
        }

        channel.invokeMethod(MethodName.handleInAppButtonTap,
                             arguments: [ArgumentName.content: String(data: inAppContent, encoding: .utf8),
                                         ArgumentName.inAppButton: String(data: inAppButton, encoding: .utf8)])
    }

    func didTapCustomButton(button: InAppMessageButton, content: InAppMessageCustomContent) {
        guard let inAppContent = try? JSONEncoder().encode(content),
              let inAppButton = try? JSONEncoder().encode(button) else {
            return
        }

        channel.invokeMethod(MethodName.handleInAppButtonTap,
                             arguments: [ArgumentName.content: String(data: inAppContent, encoding: .utf8),
                                         ArgumentName.inAppButton: String(data: inAppButton, encoding: .utf8)])
    }
}

final class InAppMessageSubscriber: InAppMessageListener {

    private let channel: FlutterMethodChannel

    init(channel: FlutterMethodChannel) {
        self.channel = channel
    }

    func didReceiveInAppMessage(content: InAppMessageContent) {
        guard let value = try? JSONEncoder().encode(content) else {
            return
        }
        channel.invokeMethod(MethodName.handleInApp,
                             arguments: [ArgumentName.content: String(data: value, encoding: .utf8)])
    }

    func didReceiveCustomInAppMessage(content: InAppMessageCustomContent) {
        guard let value = try? JSONEncoder().encode(content) else {
            return
        }

        channel.invokeMethod(MethodName.handleInApp,
                             arguments: [ArgumentName.content: String(data: value, encoding: .utf8)])
    }
}
