## 2.0.3

* [FIXED] Add workaround to exclude arm64 simulator architecture in Flutter version 2.5

## 2.0.1

* [FIXED] Parse error on Android devices

## 2.0.0

* [ADDED] Null-safety.
* [FIXED] App Inbox bridging problems.

## 1.0.1

* [FIXED] Handle null parameters for external Dart classes.

## 1.0.0

* Initial release.
