/*
 * 
 * constants.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 27/11/2020.
 * Copyright (c) 2020 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

class MethodName {
  static const collectEventWithBuilder = 'collectEventWithBuilder';
  static const collectCustomEvent = 'collectCustomEvent';

  static const customEventBuilder = 'customEventBuilder';
  static const customEventBuilder_initialize = 'customEventBuilder_initialize';
  static const customEventBuilder_dispose = 'customEventBuilder_dispose';
  static const customEventBuilder_addBoolean = 'customEventBuilder_addBoolean';
  static const customEventBuilder_addInteger = 'customEventBuilder_addInteger';
  static const customEventBuilder_addAmount = 'customEventBuilder_addAmount';
  static const customEventBuilder_addDate = 'customEventBuilder_addDate';
  static const customEventBuilder_addString = 'customEventBuilder_addString';
  static const customEventBuilder_addAttributes = 'customEventBuilder_addAttributes';

  static const setUser = 'setUser';
  static const updateUserAttributes = 'updateUserAttributes';
  static const clearUser = 'clearUser';

  static const collectAPMHTTPRecord = 'collectAPMHTTPRecord';
  static const collectAPMNetworkErrorRecord = 'collectAPMNetworkErrorRecord';

  static const addToCart = 'addToCart';
  static const addToWishList = 'addToWishList';
  static const clearCart = 'clearCart';
  static const purchase = 'purchase';
  static const removeFromCart = 'removeFromCart';
  static const search = 'search';
  static const startCheckout = 'startCheckout';
  static const removeFromWishList = 'removeFromWishList';
  static const viewCategory = 'viewCategory';
  static const viewProduct = 'viewProduct';

  static const updateConfig = 'updateConfig';
  static const enableGeofencing = 'enableGeofencing';
  static const disableGeofencing = 'disableGeofencing';
  static const updateLanguage = 'updateLanguage';

  static const collectDeeplink = 'collectDeeplink';
  static const handleDeeplink = 'handleDeeplink';
  static const handleInApp = 'handleInApp';
  static const handleInAppButtonTap = 'handleInAppButtonTap';

  static const requestNotificationAuthorizationiOS = 'requestNotificationAuthorizationiOS';
  static const handlePushEventiOS = 'handlePushEventiOS';
  static const shouldShowPushNotificationInForegroundiOS = 'shouldShowPushNotificationInForegroundiOS';

  static const enablePush = 'enablePush';

  static const startTracking = 'startTracking';
  static const stopTracking = 'stopTracking';

  static const fetchMessages = 'fetchMessages';
  static const deleteMessages = 'deleteMessages';
  static const readMessages = 'readMessages';

  static const setSuperAttribute = 'setSuperAttribute';
  static const clearSuperAttribute = 'clearSuperAttribute';

  static const deviceInfo = 'deviceInfo';

  //Component Interaction
  static const collectButtonClickEvent = 'collectButtonClickEvent';
  static const collectTextChangeEvent = 'collectTextChangeEvent';
  static const collectToggleChangeEvent = 'collectToggleChangeEvent';

  //Screen Interaction
  static const collectTouchEvent = 'collectTouchEvent';
  static const collectDoubleTapEvent = 'collectDoubleTapEvent';
  static const collectLongPressEvent = 'collectLongPressEvent';
  static const collectSwipeEvent = 'collectSwipeEvent';

  // Notification Receivers
  static const pushMessageReceived = 'pushMessageReceived';
}

class ArgumentName {
  static const name = 'name';
  static const label = 'label';
  static const viewClass = 'viewClass';
  static const eventArgs = 'eventArgs';
  static const id = 'id';
  static const value = 'value';
  static const key = 'key';
  static const amount = 'amount';
  static const currency = 'currency';

  static const customerId = 'customerId';
  static const email = 'email';
  static const phone = 'phone';
  static const nationalId = 'nationalId';
  static const firstName = 'firstName';
  static const lastName = 'lastName';
  static const dateOfBirth = 'dateOfBirth';
  static const genderIndex = 'genderIndex';
  static const attributes = 'attributes';
  static const dateAttributes = 'dateAttributes';
  static const intListAttributes = 'intListAttributes';
  static const stringListAttributes = 'stringListAttributes';

  static const url = 'url';
  static const method = 'method';
  static const statusCode = 'statusCode';
  static const requestSize = 'requestSize';
  static const responseSize = 'responseSize';
  static const duration = 'duration';
  static const headers = 'headers';
  static const connectionType = 'connectionType';
  static const success = 'success';
  static const errorType = 'errorType';
  static const errorCode = 'errorCode';
  static const errorMessage = 'errorMessage';
  static const customAttributes = 'customAttributes';
  static const exception = 'exception';
  static const message = 'message';

  static const product = 'product';
  static const products = 'products';
  static const description = 'description';
  static const brand = 'brand';
  static const quantity = 'quantity';
  static const price = 'price';
  static const variant = 'variant';
  static const category = 'category';
  static const totalCartValue = 'totalCartValue';
  static const coupon = 'coupon';
  static const query = 'query';
  static const tax = 'tax';
  static const ship = 'ship';
  static const discount = 'discount';
  static const trxId = 'trxId';
  static const paymentMethod = 'paymentMethod';

  static const goal = 'goal';
  static const appGroupIdentifier = 'appGroupIdentifier';
  static const inbox = 'inbox';
  static const storageLimit = 'storageLimit';
  static const isEnabled = 'isEnabled';
  static const recordingEnabled = 'recordingEnabled';
  static const enabledBundleIDs = 'enabledBundleIDs';
  static const snapshot = 'snapshot';
  static const inAppMessaging = 'inAppMessaging';
  static const inAppMessagingEnabled = 'inAppMessagingEnabled';
  static const recordCollectionEnabled = 'recordCollectionEnabled';
  static const recordDispatchLimit = 'recordDispatchLimit';
  static const recordStorageLimit = 'recordStorageLimit';
  static const apm = 'apm';
  static const enabled = 'enabled';
  static const screenTracking = 'screenTracking';

  static const eventCollectingDisabled = 'eventCollectingDisabled';
  static const sessionDropDuration = 'sessionDropDuration';
  static const languageCode = 'languageCode';

  static const logger = 'logger';
  static const level = 'level';
  static const writeToFile = 'writeToFile';

  static const deeplink = 'deeplink';
  static const content = 'content';
  static const inAppMessage = 'inAppMessage';
  static const inAppMessageId = 'inAppMessageId';
  static const inAppButton = 'inAppButton';
  static const title = 'title';
  static const text = 'text';
  static const language = 'language';
  static const action = 'action';
  static const buttonId = 'buttonId';

  static const notificationConfig = 'notificationConfig';
  static const smallNotificationIcon = 'smallNotificationIcon';
  static const largeNotificationIcon = 'largeNotificationIcon';
  static const defaultNotificationChannelId = 'defaultNotificationChannelId';
  static const defaultNotificationChannelName = 'defaultNotificationChannelName';

  static const pushEventTiming = 'pushEventTiming';
  static const pushActionType = 'pushActionType';
  static const pushTargetURL = 'pushTargetURL';
  static const pushAttrs = 'pushAttrs';

  static const messageIDList = 'messageIDList';
  static const messageType = 'messageType';
  static const messageStatus = 'messageStatus';
  static const from = 'from';
  static const to = 'to';
  static const isAnonymous = 'isAnonymous';

  static const type = 'type';
  static const receivedDate = 'receivedDate';
  static const expirationDate = 'expirationDate';
  static const status = 'status';
  static const payload = 'payload';

  static const deviceId = 'deviceId';
  static const deviceProperties = 'deviceProperties';

  static const left = "left";
  static const top = "top";
  static const right = "right";
  static const bottom = "bottom";

  static const accessibilityLabel = "accessibilityLabel";
  static const componentId = "componentId";
  static const className = "className";
  static const coordinates = "coordinates";
  static const screenTrackingAttributes = "screenTrackingAttributes";
  static const placeholder = "placeholder";

  static const x = "x";
  static const y = "y";
  static const touchPoint = "touchPoint";

  static const start = "start";
  static const end = "end";
  static const swipePoints = "swipePoints";
}

enum Gender { undefined, male, female, nonBinary, unknown }
