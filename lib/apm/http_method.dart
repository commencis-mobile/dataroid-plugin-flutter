enum HTTPMethod {
  POST("POST"),
  HEAD("HEAD"),
  CONNECT("CONNECT"),
  OPTIONS("OPTIONS"),
  GET("GET"),
  PATCH("PATCH"),
  PUT("PUT"),
  DELETE("DELETE"),
  TRACE("TRACE");

  final String name;

  const HTTPMethod(this.name);
}
