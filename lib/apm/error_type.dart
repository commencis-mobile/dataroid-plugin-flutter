enum ErrorType {
  unknown(0),
  noConnection(1),
  ssl(2),
  timeout(4),
  authFailure(8),
  network(16),
  parse(32),
  server(64),
  cancelled(128),
  insecureConnection(256);

  final int value;

  const ErrorType(this.value);
}
