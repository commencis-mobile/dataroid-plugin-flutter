/*
 * 
 * apm_network_record.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 30/11/2020.
 * Copyright (c) 2020 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/apm/http_method.dart';
import 'package:dataroid_plugin_flutter/constants.dart';
import 'package:dataroid_plugin_flutter/custom_attribute.dart';

import 'error_type.dart';

class APMNetworkRecord {
  String url;
  HTTPMethod method;
  int duration;
  String exception;
  ErrorType type;
  String? message;
  List<CustomAttribute>? customAttributes;
  Map<String, int>? _dateAttributes = {};
  Map<String, List<int>>? _intListAttributes = {};
  Map<String, List<String>>? _stringListAttributes = {};

  APMNetworkRecord({
    required this.url,
    required this.method,
    required this.duration,
    required this.exception,
    required this.type,
    this.message,
    this.customAttributes,
  });

  Map<String, dynamic> get toJSON {
    _parseAttributes();
    return {
      ArgumentName.url: url,
      ArgumentName.method: method.name,
      ArgumentName.duration: duration,
      ArgumentName.exception: exception,
      ArgumentName.type: type.value,
      ArgumentName.message: message,
      ArgumentName.customAttributes: (customAttributes?.map((e) => e.toJSON))?.toList(),
      ArgumentName.dateAttributes: _dateAttributes,
      ArgumentName.intListAttributes: _intListAttributes,
      ArgumentName.stringListAttributes: _stringListAttributes,
    };
  }

  void _parseAttributes() {
    customAttributes?.forEach((e) {
      if (e.value is DateTime) {
        final value = e.value as DateTime;
        _dateAttributes?[e.key] = value.millisecondsSinceEpoch;
      } else if (e.value is List<int>) {
        final value = e.value as List<int>;
        _intListAttributes?[e.key] = value;
      } else if (e.value is List<String>) {
        final value = e.value as List<String>;
        _stringListAttributes?[e.key] = value;
      }
    });
    customAttributes?.removeWhere((e) => e.value is DateTime || e.value is List<int> || e.value is List<String>);
  }
}
