/*
 * 
 * start_checkout_attributes.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 31/1/2024.
 * Copyright (c) 2024 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/commerce/commerce_event.dart';

import '../constants.dart';

class StartCheckoutAttributes extends CommerceEvent {
  final int value;
  final String currency;
  int? quantity;

  StartCheckoutAttributes({
    required this.value,
    required this.currency,
    this.quantity,
    super.attributes,
  });

  @override
  Map<String, dynamic> get toJSON => {
        ...super.toJSON,
        ArgumentName.value: value,
        ArgumentName.currency: currency,
        ArgumentName.quantity: quantity,
      };
}
