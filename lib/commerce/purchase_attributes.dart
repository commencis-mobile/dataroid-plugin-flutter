/*
 * 
 * purchase_attributes.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 31/1/2024.
 * Copyright (c) 2024 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/commerce/commerce_event.dart';
import 'package:dataroid_plugin_flutter/commerce/product.dart';
import 'package:dataroid_plugin_flutter/constants.dart';

class PurchaseAttributes extends CommerceEvent {
  final String currency;
  final double value;
  final List<Product> products;
  final bool success;
  double? tax;
  double? ship;
  double? discount;
  String? coupon;
  String? trxId;
  String? paymentMethod;
  int? quantity;
  String? errorCode;
  String? errorMessage;

  PurchaseAttributes({
    required this.currency,
    required this.value,
    required this.products,
    required this.success,
    this.tax,
    this.ship,
    this.discount,
    this.coupon,
    this.trxId,
    this.paymentMethod,
    this.quantity,
    this.errorCode,
    this.errorMessage,
    super.attributes,
  });

  @override
  Map<String, dynamic> get toJSON => {
        ...super.toJSON,
        ArgumentName.currency: currency,
        ArgumentName.value: value,
        ArgumentName.products: products.map((e) => e.toJSON).toList(),
        ArgumentName.success: success,
        ArgumentName.tax: tax,
        ArgumentName.ship: ship,
        ArgumentName.discount: discount,
        ArgumentName.coupon: coupon,
        ArgumentName.trxId: trxId,
        ArgumentName.paymentMethod: paymentMethod,
        ArgumentName.quantity: quantity,
        ArgumentName.errorCode: errorCode,
        ArgumentName.errorMessage: errorMessage,
      };
}
