/*
 * 
 * view_category_attributes.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 31/1/2024.
 * Copyright (c) 2024 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/commerce/commerce_event.dart';

import '../constants.dart';

class ViewCategoryAttributes extends CommerceEvent {
  final String category;

  ViewCategoryAttributes({
    required this.category,
    super.attributes,
  });

  @override
  Map<String, dynamic> get toJSON => {
        ...super.toJSON,
        ArgumentName.category: category,
      };
}
