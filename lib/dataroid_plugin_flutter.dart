import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dataroid_plugin_flutter/apm/apm_http_record.dart';
import 'package:dataroid_plugin_flutter/apm/apm_network_record.dart';
import 'package:dataroid_plugin_flutter/commerce/add_to_cart_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/add_to_wishlist_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/clear_cart_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/purchase_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/remove_from_cart_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/remove_from_wishlist_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/search_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/start_checkout_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/view_category_attributes.dart';
import 'package:dataroid_plugin_flutter/commerce/view_product_attributes.dart';
import 'package:dataroid_plugin_flutter/component_interaction/screen_interaction/double_tap_attributes.dart';
import 'package:dataroid_plugin_flutter/component_interaction/screen_interaction/long_press_attributes.dart';
import 'package:dataroid_plugin_flutter/component_interaction/screen_interaction/swipe_attributes.dart';
import 'package:dataroid_plugin_flutter/component_interaction/screen_interaction/touch_attributes.dart';
import 'package:dataroid_plugin_flutter/component_interaction/text_change_attributes.dart';
import 'package:dataroid_plugin_flutter/component_interaction/toggle_change_attributes.dart';
import 'package:dataroid_plugin_flutter/constants.dart';
import 'package:dataroid_plugin_flutter/custom_event.dart';
import 'package:dataroid_plugin_flutter/dataroid_plugin_config.dart';
import 'package:dataroid_plugin_flutter/deeplink_referral/deeplink_attributes.dart';
import 'package:dataroid_plugin_flutter/inbox/inbox_message.dart';
import 'package:dataroid_plugin_flutter/inbox/inbox_query.dart';
import 'package:dataroid_plugin_flutter/push/inapp_button.dart';
import 'package:dataroid_plugin_flutter/push/push_models.dart';
import 'package:dataroid_plugin_flutter/screen_tracker.dart';
import 'package:dataroid_plugin_flutter/super_attributes/super_attribute.dart';
import 'package:dataroid_plugin_flutter/user.dart';
import 'package:flutter/services.dart';

import 'component_interaction/button_click_attributes.dart';

/// The delegate interface for Dataroid SDK events
///
/// Create a class and initialize [DatroidPluginFlutter] instance with that.
abstract class DataroidPluginFlutterDelegate {
  void handleInAppMessageDeeplink(String deeplink);

  void handleInApp(String content);

  void handleInAppButtonTap(InAppButton button, String content);

  void handlePushEventiOS(
    PushActionType type,
    PushEventTiming timing,
    String targetURL,
    Map attributes,
  );

  bool shouldShowNotificationInForeground(Map<String, dynamic> userInfo);
}

class DataroidPluginFlutter {
  MethodChannel channel = const MethodChannel('dataroid_plugin_flutter');
  DataroidPluginFlutterDelegate? delegate;

  static final DataroidPluginFlutter _shared = DataroidPluginFlutter._internal();

  factory DataroidPluginFlutter() {
    return _shared;
  }

  DataroidPluginFlutter._internal() {
    channel.setMethodCallHandler(_nativeMethodHandler);
  }

  /// Unique device identifier used by client
  Future<String> get deviceId async {
    final deviceInfo = await channel.invokeMethod(MethodName.deviceInfo);
    return deviceInfo[ArgumentName.deviceId];
  }

  /// Device properties
  Future<String> get deviceProperties async {
    final deviceInfo = await channel.invokeMethod(MethodName.deviceInfo);
    return deviceInfo[ArgumentName.deviceProperties];
  }

  /// Collects custom event
  Future<void> collectCustomEvent(CustomEvent event) async {
    await channel.invokeMethod(MethodName.collectCustomEvent, event.toJSON);
  }

  /// Sets the [user] as current SDK user.
  Future<void> setUser(
    User user,
  ) async {
    await channel.invokeMethod(
      MethodName.setUser,
      user.toJSON,
    );
  }

  /// Updates the current user's properties.
  Future<void> updateUser({
    String? email,
    String? phone,
    String? nationalId,
    String? firstName,
    String? lastName,
    DateTime? dateOfBirth,
    Gender? gender,
    Map<String, dynamic>? attributes,
  }) async {
    Map<String, int>? _dateAttributes = {};

    attributes?.entries.forEach((e) {
      if (e.value is DateTime) {
        final value = e.value as DateTime;
        _dateAttributes[e.key] = value.millisecondsSinceEpoch;
      }
    });
    attributes?.removeWhere((key, value) => value is DateTime);

    var args = {
      ArgumentName.email: email,
      ArgumentName.phone: phone,
      ArgumentName.nationalId: nationalId,
      ArgumentName.firstName: firstName,
      ArgumentName.lastName: lastName,
      ArgumentName.dateOfBirth: dateOfBirth?.millisecondsSinceEpoch,
      ArgumentName.genderIndex: gender?.index,
      ArgumentName.attributes: attributes,
      ArgumentName.dateAttributes: _dateAttributes,
    };
    await channel.invokeMethod(
      MethodName.updateUserAttributes,
      args,
    );
  }

  /// Clears current user information.
  Future<void> clearUser() async {
    await channel.invokeMethod(MethodName.clearUser);
  }

  /// Collects HTTP Record event.
  Future<void> collectAPMHTTPRecord(APMHTTPRecord record) async {
    await channel.invokeMethod(
      MethodName.collectAPMHTTPRecord,
      record.toJSON,
    );
  }

  /// Collects Network Error event.
  Future<void> collectAPMNetworkErrorRecord(APMNetworkRecord record) async {
    await channel.invokeMethod(
      MethodName.collectAPMNetworkErrorRecord,
      record.toJSON,
    );
  }

  /// Collects add to cart event with given [product].
  Future<void> addToCart(AddToCartAttributes addToCartAttributes) async {
    await channel.invokeMethod(MethodName.addToCart, addToCartAttributes.toJSON);
  }

  /// Collects add to wish list with given [product].
  Future<void> addToWishList(AddToWishlistAttributes addToWishlistAttributes) async {
    await channel.invokeMethod(MethodName.addToWishList, addToWishlistAttributes.toJSON);
  }

  /// Collects clear cart event with given attributes.
  Future<void> clearCart(ClearCartAttributes clearCartAttributes) async {
    await channel.invokeMethod(MethodName.clearCart, clearCartAttributes.toJSON);
  }

  /// Collects purchase event with given [products].
  Future<void> purchase(PurchaseAttributes purchaseAttributes) async {
    await channel.invokeMethod(MethodName.purchase, purchaseAttributes.toJSON);
  }

  /// Collects remove to cart event with given [product].
  Future<void> removeFromCart(RemoveFromCartAttributes removeFromCartAttributes) async {
    await channel.invokeMethod(MethodName.removeFromCart, removeFromCartAttributes.toJSON);
  }

  /// Collects remove from wish list event with given [product].
  Future<void> removeFromWishList(RemoveFromWishlistAttributes removeFromWishlistAttributes) async {
    await channel.invokeMethod(MethodName.removeFromWishList, removeFromWishlistAttributes.toJSON);
  }

  /// Collects search event with given [query].
  Future<void> search(SearchAttributes searchAttributes) async {
    await channel.invokeMethod(MethodName.search, searchAttributes.toJSON);
  }

  /// Collects start to checkout event.
  Future<void> startCheckout(StartCheckoutAttributes startCheckoutAttributes) async {
    await channel.invokeMethod(MethodName.startCheckout, startCheckoutAttributes.toJSON);
  }

  /// Collects view [category] event.
  Future<void> viewCategory(ViewCategoryAttributes categoryAttributes) async {
    await channel.invokeMethod(MethodName.viewCategory, categoryAttributes.toJSON);
  }

  /// Collects view [product] event.
  Future<void> viewProduct(ViewProductAttributes productAttributes) async {
    await channel.invokeMethod(MethodName.viewProduct, productAttributes.toJSON);
  }

  /// Triggers Dataroid's FCM MessageReceived class. Android only.
  Future<bool> pushMessageReceived(Map<String, String> data) async {
    if(Platform.isAndroid) {
      var result = await channel.invokeMethod(MethodName.pushMessageReceived, data);
      return result;
    }
    return false;
  }

  /// Updates configuration objects after initialization
  ///
  /// If any argument is null, or not provided; that value won't be written
  /// to the current config.
  ///
  /// [eventCollectingDisabled] sets the event collecting status.
  /// [sessionDropDuration] is the duration that needs to pass to close the previous session when app is in background.
  Future<void> updateConfig({
    double? sessionDropDuration,
    DataroidGoalConfig? goal,
    DataroidSnapshotConfig? snapshot,
    DataroidInAppMessagingConfig? inAppMessaging,
    DataroidAPMConfig? apm,
    DataroidScreenTrackingConfig? screenTracking,
    NotificationConfig? notificationConfig,
    LoggerConfig? logger,
  }) async {
    await channel.invokeMethod(MethodName.updateConfig, {
      ArgumentName.sessionDropDuration: sessionDropDuration,
      ArgumentName.goal: goal?.toJSON,
      ArgumentName.snapshot: snapshot?.toJSON,
      ArgumentName.inAppMessaging: inAppMessaging?.toJSON,
      ArgumentName.apm: apm?.toJSON,
      ArgumentName.screenTracking: screenTracking?.toJSON,
      ArgumentName.notificationConfig: notificationConfig?.toJSON,
      ArgumentName.logger: logger?.toJSON,
    });
  }

  /// Updates the current language.
  Future<void> updateLanguage({
    required String languageCode,
  }) async {
    await channel.invokeMethod(MethodName.updateLanguage, {
      ArgumentName.languageCode: languageCode,
    });
  }

  /// Enables geofencing feature. Permission requests must be handled by host application.
  Future<void> enableGeofencing() async {
    await channel.invokeMethod(MethodName.enableGeofencing);
  }

  /// Disables geofencing feature.
  Future<void> disableGeofencing() async {
    await channel.invokeMethod(MethodName.disableGeofencing);
  }

  /// Collects deeplink events.
  ///
  /// Host app must call this when the application routes to a deeplink.
  Future<void> collectDeeplink(DeeplinkAttributes deeplinkAttributes) async {
    await channel.invokeMethod(MethodName.collectDeeplink, deeplinkAttributes.toJSON);
  }

  /// Starts tracking the page with given [name] and [label].
  Future<void> startTracking(ScreenTracker tracker) async {
    await channel.invokeMethod(MethodName.startTracking, tracker.toJSON);
  }

  /// Stops tracking the page with given [name] and [label].
  Future<void> stopTracking(ScreenTracker tracker) async {
    await channel.invokeMethod(MethodName.stopTracking, tracker.toJSON);
  }

  /// [iOS] Requests authorization from user to present notifications.
  Future<void> requestNotificationAuthorization() async {
    await channel.invokeMethod(MethodName.requestNotificationAuthorizationiOS);
  }

  /// [Android] Enables push notifications.
  Future<void> enablePush() async {
    await channel.invokeMethod(MethodName.enablePush);
  }

  /// Retrieves inbox messages from database with an optional [query].
  Future<List<InboxMessage>> fetchMessages({
    InboxQuery? query,
  }) async {
    if (Platform.isIOS) {
      final messages = await channel.invokeMethod(
        MethodName.fetchMessages,
        query?.toJSON,
      );
      return List<InboxMessage>.from(
        messages.map((e) => InboxMessage(e)),
      );
    } else if (Platform.isAndroid) {
      final messagesString = await channel.invokeMethod(
        MethodName.fetchMessages,
        query?.toJSON,
      );
      final List<dynamic> messageList = json.decode(messagesString);
      return messageList.map((e) => InboxMessage(e)).toList();
    } else {
      throw PlatformException(
        code: "PlatformException",
        message: "Unsupported platform!",
      );
    }
  }

  /// Deletes inbox messages associated with given IDs.
  Future<bool> deleteMessages(List<String> messageIDs) async {
    return await channel.invokeMethod(MethodName.deleteMessages, {
      ArgumentName.messageIDList: messageIDs,
    });
  }

  /// Marks inbox messages associated with given IDs as read.
  Future<bool> readMessages(List<String> messageIDs) async {
    return await channel.invokeMethod(MethodName.readMessages, {
      ArgumentName.messageIDList: messageIDs,
    });
  }

  /// Sets or updates the [superAttribute].
  Future<void> setSuperAttribute(
    SuperAttribute superAttribute,
  ) async {
    await channel.invokeMethod(
      MethodName.setSuperAttribute,
      superAttribute.toJSON,
    );
  }

  /// Clears the [superAttribute] using it's key value.
  Future<void> clearSuperAttribute(
    SuperAttribute superAttribute,
  ) async {
    await channel.invokeMethod(
      MethodName.clearSuperAttribute,
      superAttribute.toJSON,
    );
  }

  /// Collects the [buttonClickEvent].
  Future<void> collectButtonClick(ButtonClickAttributes buttonClickAttributes) async {
    await channel.invokeMethod(
      MethodName.collectButtonClickEvent,
      buttonClickAttributes.toJSON,
    );
  }

  /// Collects the [textChangeEvent].
  Future<void> collectTextChange(TextChangeAttributes textChangeAttributes) async {
    await channel.invokeMethod(
      MethodName.collectTextChangeEvent,
      textChangeAttributes.toJSON,
    );
  }

  /// Collects the [toggleChangeEvent].
  Future<void> collectToggleChange(ToggleChangeAttributes toggleChangeAttributes) async {
    await channel.invokeMethod(
      MethodName.collectToggleChangeEvent,
      toggleChangeAttributes.toJSON,
    );
  }

  /// Collects the [touchEvent].
  Future<void> collectTouch(TouchAttributes touchAttributes) async {
    await channel.invokeMethod(
      MethodName.collectTouchEvent,
      touchAttributes.toJSON,
    );
  }

  /// Collects the [doubleTapEvent].
  Future<void> collectDoubleTap(DoubleTapAttributes doubleTapAttributes) async {
    await channel.invokeMethod(
      MethodName.collectDoubleTapEvent,
      doubleTapAttributes.toJSON,
    );
  }

  /// Collects the [longPressEvent].
  Future<void> collectLongPress(LongPressAttributes longPressAttributes) async {
    await channel.invokeMethod(
      MethodName.collectLongPressEvent,
      longPressAttributes.toJSON,
    );
  }

  /// Collects the [swipeEvent].
  Future<void> collectSwipe(SwipeAttributes swipeAttributes) async {
    await channel.invokeMethod(
      MethodName.collectSwipeEvent,
      swipeAttributes.toJSON,
    );
  }

  Future<dynamic> _nativeMethodHandler(MethodCall methodCall) async {
    if (delegate == null) {
      print('[DATAROID/FLUTTER] Received native callback, but the delegate is null!');
      return false;
    }
    final args = methodCall.arguments;
    switch (methodCall.method) {
      case MethodName.handleDeeplink:
        final deeplink = args[ArgumentName.deeplink] ?? "";
        delegate?.handleInAppMessageDeeplink(deeplink);
        return true;
      case MethodName.handleInAppButtonTap:
        delegate?.handleInAppButtonTap(
          InAppButton(json.decode(args[ArgumentName.inAppButton])),
          args[ArgumentName.content],
        );
        return true;
      case MethodName.handleInApp:
        delegate?.handleInApp(args[ArgumentName.content]);
        return true;
      case MethodName.handlePushEventiOS:
        {
          final actionType = PushActionType.values[args[ArgumentName.pushActionType] as int];
          final timing = PushEventTiming.values[args[ArgumentName.pushEventTiming] as int];
          final targetURL = (args[ArgumentName.pushTargetURL] as String?) ?? "";
          final attributes = args[ArgumentName.pushAttrs];

          delegate?.handlePushEventiOS(actionType, timing, targetURL, attributes);
          return true;
        }
      case MethodName.shouldShowPushNotificationInForegroundiOS:
        final userInfo = args[ArgumentName.value];
        return delegate?.shouldShowNotificationInForeground(userInfo);
      default:
        throw MissingPluginException('notImplemented');
    }
  }
}
