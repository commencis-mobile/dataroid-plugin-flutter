/*
 * 
 * push_models.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 09/12/2020.
 * Copyright (c) 2020 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

enum PushEventTiming {
  receivedWhenTerminatedAndOpenedTheAp,
  receivedInBackgroundAndOpenedTheApp,
  receivedInBackground,
  receivedInForeground,
}

enum PushActionType {
  nothing,
  openApp,
  gotoURL,
  gotoDeeplink,
}
