/*
 * 
 * inbox_message.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 10/12/2020.
 * Copyright (c) 2020 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/constants.dart';

enum InboxMessageType { push, inApp }

enum InboxMessageStatus { unread, read, dismissed }

class InboxMessage {
  String? id;
  InboxMessageType? type;
  DateTime? receivedDate;
  DateTime? expirationDate;
  String? customerId;
  InboxMessageStatus? status;
  String? payload;

  InboxMessage(Map<dynamic, dynamic> json) {
    id = json[ArgumentName.id] as String?;
    type =
        InboxMessageType.values[(json[ArgumentName.messageType] as int?) ?? 0];
    status = InboxMessageStatus
        .values[(json[ArgumentName.messageStatus] as int?) ?? 0];
    customerId = json[ArgumentName.customerId] as String?;
    final receivedInterval =
        double.tryParse(json[ArgumentName.receivedDate].toString());
    if (receivedInterval != null) {
      receivedDate =
          DateTime.fromMillisecondsSinceEpoch(receivedInterval.toInt());
    }
    final expirationInterval =
        double.tryParse(json[ArgumentName.expirationDate].toString());
    if (expirationInterval != null) {
      expirationDate =
          DateTime.fromMillisecondsSinceEpoch(expirationInterval.toInt());
    }
    payload = json[ArgumentName.payload] as String?;
  }
}
