/*
 * 
 * swipe_points.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 31/1/2024.
 * Copyright (c) 2024 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/component_interaction/screen_interaction/touch_point.dart';
import 'package:dataroid_plugin_flutter/constants.dart';

class SwipePoints {
  final TouchPoint start;
  final TouchPoint end;

  SwipePoints({required this.start, required this.end});

  Map<String, dynamic> get toJSON {
    return {
      ArgumentName.start: start.toJSON,
      ArgumentName.end: end.toJSON,
    };
  }
}
