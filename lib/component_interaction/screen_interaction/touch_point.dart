/*
 * 
 * touch_point.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 31/1/2024.
 * Copyright (c) 2024 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/constants.dart';

class TouchPoint {
  final int x;
  final int y;

  TouchPoint({required this.x, required this.y});

  Map<String, dynamic> get toJSON {
    return {
      ArgumentName.x: x,
      ArgumentName.y: y,
    };
  }
}
