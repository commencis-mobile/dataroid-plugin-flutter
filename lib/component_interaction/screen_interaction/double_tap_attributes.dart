/*
 * 
 * double_tap_attributes.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 31/1/2024.
 * Copyright (c) 2024 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/component_interaction/component_attributes.dart';
import 'package:dataroid_plugin_flutter/component_interaction/screen_interaction/touch_point.dart';
import 'package:dataroid_plugin_flutter/constants.dart';

class DoubleTapAttributes extends ComponentAttributes {
  final TouchPoint touchPoint;

  DoubleTapAttributes({
    required this.touchPoint,
    super.accessibilityLabel,
    super.componentId,
    required super.className,
    super.coordinates,
    super.screenTracker,
  });

  @override
  Map<String, dynamic> get toJSON => {
        ArgumentName.touchPoint: touchPoint.toJSON,
        ArgumentName.accessibilityLabel: accessibilityLabel,
        ArgumentName.componentId: componentId,
        ArgumentName.className: className,
        ArgumentName.coordinates: coordinates?.toJSON,
        ArgumentName.screenTrackingAttributes: screenTracker?.toJSON,
      };
}
