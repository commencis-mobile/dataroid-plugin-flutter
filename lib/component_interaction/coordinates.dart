/*
 * 
 * coordinates.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 30/1/2024.
 * Copyright (c) 2024 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/constants.dart';

class Coordinates {
  final int left;
  final int top;
  final int right;
  final int bottom;

  Coordinates({required this.left, required this.top, required this.right, required this.bottom});

  Map<String, dynamic> get toJSON {
    return {
      ArgumentName.left: left,
      ArgumentName.top: top,
      ArgumentName.right: right,
      ArgumentName.bottom: bottom,
    };
  }
}
