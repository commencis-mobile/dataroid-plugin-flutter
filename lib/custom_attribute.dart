/*
 * 
 * custom_attribute.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 30/11/2020.
 * Copyright (c) 2020 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/constants.dart';

class CustomAttribute {
  final String key;
  final dynamic value;
  CustomAttribute({required this.key, required this.value});

  Map<String, dynamic> get toJSON => {
        ArgumentName.key: key,
        ArgumentName.value: value,
      };
}
