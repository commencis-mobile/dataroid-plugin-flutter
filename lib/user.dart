/*
 * 
 * user.dart
 * Dataroid-Plugin-Flutter
 * 
 * Created on 27/11/2020.
 * Copyright (c) 2020 Commencis. All rights reserved.
 * 
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 * 
 */

import 'package:dataroid_plugin_flutter/constants.dart';

class User {
  final String customerId;
  String? email;
  String? phone;
  String? nationalId;
  String? firstName;
  String? lastName;
  DateTime? dateOfBirth;
  Gender? gender;
  Map<String, dynamic>? attributes;
  Map<String, int>? _dateAttributes = {};

  User({
    required this.customerId,
  });

  Map<String, dynamic> get toJSON {
    _parseAttributes();
    return {
      ArgumentName.customerId: customerId,
      ArgumentName.email: email,
      ArgumentName.phone: phone,
      ArgumentName.nationalId: nationalId,
      ArgumentName.firstName: firstName,
      ArgumentName.lastName: lastName,
      ArgumentName.dateOfBirth: dateOfBirth?.millisecondsSinceEpoch,
      ArgumentName.genderIndex: gender?.index,
      ArgumentName.attributes: attributes,
      ArgumentName.dateAttributes: _dateAttributes,
    };
  }

  void _parseAttributes() {
    attributes?.entries.forEach((e) {
      if (e.value is DateTime) {
        final value = e.value as DateTime;
        _dateAttributes?[e.key] = value.millisecondsSinceEpoch;
      }
    });
    attributes?.removeWhere((key, value) => value is DateTime);
  }
}
