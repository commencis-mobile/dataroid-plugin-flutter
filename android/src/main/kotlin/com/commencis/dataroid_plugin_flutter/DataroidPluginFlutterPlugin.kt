package com.commencis.dataroid_plugin_flutter

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.net.Uri
import android.os.Handler
import android.os.Looper
import com.commencis.appconnect.sdk.AppConnect
import com.commencis.appconnect.sdk.analytics.screentracking.AppConnectScreenTrackingConfig
import com.commencis.appconnect.sdk.annotations.Gender
import com.commencis.appconnect.sdk.annotations.HttpMethod
import com.commencis.appconnect.sdk.apm.AppConnectAPMConfig
import com.commencis.appconnect.sdk.apm.NetworkErrorErrorType
import com.commencis.appconnect.sdk.autocollect.component.Coordinates
import com.commencis.appconnect.sdk.autocollect.gesture.TouchPoint
import com.commencis.appconnect.sdk.core.event.AttributeBuilder
import com.commencis.appconnect.sdk.core.event.Attributes
import com.commencis.appconnect.sdk.core.event.ButtonClickAttributes
import com.commencis.appconnect.sdk.core.event.CartAttributes
import com.commencis.appconnect.sdk.core.event.ClearCartAttributes
import com.commencis.appconnect.sdk.core.event.CustomerAttributes
import com.commencis.appconnect.sdk.core.event.DeeplinkLaunchedAttributes
import com.commencis.appconnect.sdk.core.event.DoubleTapAttributes
import com.commencis.appconnect.sdk.core.event.HttpCallAttributes
import com.commencis.appconnect.sdk.core.event.LongPressAttributes
import com.commencis.appconnect.sdk.core.event.NetworkErrorAttributes
import com.commencis.appconnect.sdk.core.event.Product
import com.commencis.appconnect.sdk.core.event.PurchaseAttributes
import com.commencis.appconnect.sdk.core.event.ScreenTrackingAttributes
import com.commencis.appconnect.sdk.core.event.SearchAttributes
import com.commencis.appconnect.sdk.core.event.StartCheckoutAttributes
import com.commencis.appconnect.sdk.core.event.SwipeAttributes
import com.commencis.appconnect.sdk.core.event.TextChangeAttributes
import com.commencis.appconnect.sdk.core.event.ToggleChangeAttributes
import com.commencis.appconnect.sdk.core.event.TouchAttributes
import com.commencis.appconnect.sdk.core.event.ViewCategoryAttributes
import com.commencis.appconnect.sdk.core.event.ViewProductAttributes
import com.commencis.appconnect.sdk.core.event.WishListAttributes
import com.commencis.appconnect.sdk.iamessaging.AppConnectInAppMessagingConfig
import com.commencis.appconnect.sdk.inbox.InboxMessage
import com.commencis.appconnect.sdk.inbox.InboxQuery
import com.commencis.appconnect.sdk.mobileservices.protocol.PushRegistrationResult
import com.commencis.appconnect.sdk.network.AppConnectJsonConverter
import com.commencis.appconnect.sdk.network.networkconfig.AppConnectCertificatePinningConfig
import com.commencis.appconnect.sdk.network.networkconfig.AppConnectNetworkConfig
import com.commencis.appconnect.sdk.push.AppConnectPushNotificationConverter
import com.commencis.appconnect.sdk.push.InboxMessageStatus
import com.commencis.appconnect.sdk.push.InboxMessageType
import com.commencis.appconnect.sdk.push.RemoteNotificationHandler
import com.commencis.appconnect.sdk.registry.AppConnectInstanceRegistry
import com.commencis.appconnect.sdk.util.AppConnectLogConfig
import com.commencis.appconnect.sdk.util.logging.ConnectCommonLog
import com.commencis.appconnect.sdk.util.time.SystemCurrentTimeProvider
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import org.json.JSONArray
import java.io.File
import java.math.BigDecimal
import java.sql.Timestamp
import java.util.*
enum class LogLevel(val level: Int) {
    NO_LOG(0),
    VERBOSE(2),
    DEBUG(3),
    WARN(5),
    ERROR(6)
}
  class DataroidPluginConfig(val sdkKey: String, val serverURL: String) {
  var isEventCollectingDisabled: Boolean? = false
  var isAPMEnabled: Boolean? = null
  var isInAppMessagingEnabled: Boolean? = null
  var isScreenTrackingEnabled: Boolean? = null
  var logLevel: LogLevel? = null
  var isFileLoggingEnabled: Boolean? = null
  var eventStorageLimit: Int? = null
  var eventDispatchLimit: Int? = null
  var languageCode: String? = null
  var pinningEndpoint: String? = null
  var pinningKey: String? = null
  var sessionDropDuration: Double? = null
}

/** DataroidPluginFlutterPlugin */
class DataroidPluginFlutterPlugin: FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  private lateinit var context: Context
  private var activity: Activity? = null
  private fun convertToHttpMethod(methodName: String?) : String? {
    return when(methodName) {
      "POST" -> HttpMethod.POST
      "HEAD" -> HttpMethod.HEAD
      "CONNECT" -> HttpMethod.CONNECT
      "OPTIONS" -> HttpMethod.OPTIONS
      "GET" -> HttpMethod.GET
      "PATCH" ->HttpMethod.PATCH
      "PUT" -> HttpMethod.PUT
      "DELETE" ->HttpMethod.DELETE
      "TRACE" ->HttpMethod.TRACE
      else -> null
    }
  }

  override fun onAttachedToEngine( flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    context = flutterPluginBinding.applicationContext

    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "dataroid_plugin_flutter")
    channel.setMethodCallHandler(this)

    appConnectInstance.inAppMessaging.subscribeToMessages {
      channel.invokeMethod(METHOD_HANDLE_IN_APP, mapOf(ARGUMENT_CONTENT to mapOf(ARGUMENT_TITLE to it.title, ARGUMENT_TEXT to it.text, ARGUMENT_LANGUAGE to it.language)));
    }
// TO-DO
    /*appConnectInstance.inAppMessaging.setOnInAppMessageButtonClickListener { inAppMessage, appConnectButton ->
      val buttonRet = mapOf(ARGUMENT_TEXT to appConnectButton.text, ARGUMENT_ACTION to appConnectButton.action, ARGUMENT_BUTTON_ID to appConnectButton.buttonId)
      val messageRet = mapOf(ARGUMENT_IN_APP_MESSAGE_ID to inAppMessage.inappMessageId)
      channel.invokeMethod(METHOD_HANDLE_IN_APP_BUTTON_TAP, mapOf(ARGUMENT_IN_APP_BUTTON to buttonRet, ARGUMENT_IN_APP_MESSAGE to messageRet))
    }*/

  }

  override fun onMethodCall( call: MethodCall,  result: Result) {
    Handler(Looper.getMainLooper()).post {
      try {
        val arguments = (call.arguments as? Map<String, Any>) ?: emptyMap<String, Any>()
        when (call.method) {
            METHOD_COLLECT_CUSTOM_EVENT -> {
              handleCollectCustomEvent(arguments, result)
            }
            METHOD_SET_USER -> {
              handleSetUser(arguments, result)
            }
            METHOD_UPDATE_USER_ATTRIBUTES -> {
              handleUpdateUser(arguments, result)
            }
            METHOD_CLEAR_USER -> {
              handleClearUser(result)
            }
            METHOD_COLLECT_APMHTTP_RECORD -> {
              handleCollectAPMHTTPRecord(arguments, result)
            }
            METHOD_COLLECT_APM_NETWORK_ERROR_RECORD -> {
              handleCollectAPMNetworkRecord(arguments, result)
            }
            METHOD_ADD_TO_CART -> {
              handleAddToCart(arguments, result)
            }
            METHOD_ADD_TO_WISH_LIST -> {
              handleAddToWishList(arguments, result)
            }
            METHOD_CLEAR_CART -> {
              handleClearCart(arguments, result)
            }
            METHOD_PURCHASE -> {
              handlePurchase(arguments, result)
            }
            METHOD_REMOVE_FROM_CART -> {
              handleRemoveFromCart(arguments, result)
            }
            METHOD_SEARCH -> {
              handleSearch(arguments, result)
            }
            METHOD_START_CHECKOUT -> {
              handleStartCheckout(arguments, result)
            }
            METHOD_REMOVE_FROM_WISH_LIST -> {
              handleRemoveFromWishList(arguments, result)
            }
            METHOD_VIEW_CATEGORY -> {
              handleViewCategory(arguments, result)
            }
            METHOD_VIEW_PRODUCT -> {
              handleViewProduct(arguments, result)
            }
            METHOD_UPDATE_CONFIG -> {
              handleUpdateConfig(arguments, result)
            }
            METHOD_ENABLE_GEOFENCING -> {
              handleEnableGeofencing(result)
            }
            METHOD_DISABLE_GEOFENCING -> {
              handleDisableGeofencing(result)
            }
            METHOD_UPDATE_LANGUAGE -> {
              handleUpdateLanguage(arguments, result)
            }
            METHOD_ENABLE_PUSH -> {
              handleEnablePush(result)
            }
            METHOD_START_TRACKING -> {
              handleStartTracking(arguments, result)
            }
            METHOD_STOP_TRACKING -> {
              handleStopTracking(arguments, result)
            }
            METHOD_FETCH_MESSAGES -> {
              handleFetchInboxMessages(arguments, result)
            }
            METHOD_DELETE_MESSAGES -> {
              handleDeleteInboxMessages(arguments, result)
            }
            METHOD_READ_MESSAGES -> {
              handleReadInboxMessages(arguments, result)
            }
            METHOD_SET_SUPER_ATTRIBUTE -> {
              handleSetSuperAttribute(arguments, result)
            }
            METHOD_CLEAR_SUPER_ATTRIBUTE -> {
              handleClearSuperAttribute(arguments, result)
            }
            METHOD_COLLECT_DEEPLINK -> {
              handleCollectDeeplink(arguments, result)
            }
            METHOD_COLLECT_BUTTON_CLICK_EVENT -> {
              handleCollectButtonClickEvent(arguments, result)
            }
            METHOD_COLLECT_TEXT_CHANGE_EVENT -> {
              handleCollectTextChangeEvent(arguments, result)
            }
            METHOD_COLLECT_TOGGLE_CHANGE_EVENT -> {
              handleCollectToggleChangeEvent(arguments, result)
            }
            METHOD_COLLECT_TOUCH_EVENT -> {
              handleCollectTouchEvent(arguments, result)
            }
            METHOD_COLLECT_DOUBLE_TAP_EVENT -> {
              handleCollectDoubleTapEvent(arguments, result)
            }
            METHOD_COLLECT_LONG_PRESS_EVENT -> {
              handleCollectLongPressEvent(arguments, result)
            }
            METHOD_COLLECT_SWIPE_EVENT -> {
              handleCollectSwipeEvent(arguments, result)
            }
            METHOD_PUSH_MESSAGE_RECEIVED -> {
              handlePushMessageReceived(arguments, result)
            }
            else -> {
              result.success(false)
            }
          }
      } catch (e: Exception) {
        result.error("-1", e.message, null)
      }
    }
  }

  override fun onDetachedFromEngine( binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  @Throws(MissingArgumentException::class)
  private fun handleCollectCustomEvent(arguments: Map<String, Any>,  result: Result) {
    val eventName = arguments[ARGUMENT_NAME] as? String
    if (eventName.isNullOrEmpty()) {
      throw MissingArgumentException()
    }

    val attributes = Attributes().apply {
      (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        var value = parseLong(it.value)

        if (value != null) {
          val date = Date(Timestamp(value).time)
          put(it.key, date)
        }
      }

      (arguments[ARGUMENT_INT_LIST_ATTRIBUTES] as? Map<String, Any?>)?.forEach {
        val typedVal = (it.value as? List<Int>) ?: listOf<Int>()
        var value = IntArray(typedVal.size)
        for (i in typedVal.indices) {
          value[i] = typedVal[i]
        }
        put(it.key, value)

      }

      (arguments[ARGUMENT_STRING_LIST_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        val typedVal = (it.value as? List<String>) ?: listOf<String>()
        var value = Array<String?>(typedVal.size) {null}
        for (i in typedVal.indices) {
          value[i] = typedVal[i]
        }
        put(it.key, value)
      }

      (arguments[ARGUMENT_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        when(it.value) {
          is Int -> put(it.key, it.value as Int?)
          is Double -> put(it.key, it.value as Double?)
          is Float -> put(it.key, it.value as Float?)
          is Boolean -> put(it.key, it.value as Boolean?)
          is String -> put(it.key, it.value as String?)
          else -> {}
        }
      }
    }

    appConnectInstance.collectEvent(eventName, attributes)
    result.success(true)
  }

  private fun parseCustomerAttributes(key: String, value : Any, customerAttributes : CustomerAttributes) {
    when(value) {
      is Boolean -> customerAttributes.put(key, value as Boolean?)
      is Double -> customerAttributes.put(key, value as Double?)
      is Float -> customerAttributes.put(key, value as Float?)
      is Int -> customerAttributes.put(key, value as Int?)
      is String -> customerAttributes.put(key, value as String?)
      else -> {}
    }
  }

  @Throws(MissingArgumentException::class)
  private fun handleSetUser(arguments: Map<String, Any>,  result: Result) {
    val customerId = arguments[ARGUMENT_CUSTOMER_ID] as? String
    if (customerId.isNullOrEmpty()) {
      throw MissingArgumentException()
    }

    val customerAttributes = CustomerAttributes()
    (arguments[ARGUMENT_EMAIL] as? String)?.let {
      customerAttributes.setEmail(it)
    }
    (arguments[ARGUMENT_PHONE] as? String)?.let {
      customerAttributes.setPhoneNumber(it)
    }
    (arguments[ARGUMENT_NATIONAL_ID] as? String)?.let {
      customerAttributes.setNationalId(it)
    }
    (arguments[ARGUMENT_FIRST_NAME] as? String)?.let {
      customerAttributes.setFirstName(it)
    }
    (arguments[ARGUMENT_LAST_NAME] as? String)?.let {
      customerAttributes.setLastName(it)
    }
    parseLong(arguments[ARGUMENT_DATE_OF_BIRTH])?.let {
      val date = Date(Timestamp(it).time)
      customerAttributes.setDateOfBirth(date)
    }
    (arguments[ARGUMENT_GENDER_INDEX] as? Int)?.let {
      when (it) {
        1 -> customerAttributes.setGender(Gender.MALE)
        2 -> customerAttributes.setGender(Gender.FEMALE)
        3 -> customerAttributes.setGender(Gender.NONBINARY)
        4 -> customerAttributes.setGender(Gender.UNKNOWN)
        else -> { }
      }
    }
    (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      var value = parseLong(it.value)

      if (value != null) {
        val date = Date(Timestamp(value).time)
        customerAttributes.put(it.key, date)
      }

    }

    (arguments[ARGUMENT_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        parseCustomerAttributes(it.key, it.value, customerAttributes)
    }
    appConnectInstance.setCustomer(customerId, customerAttributes)
    result.success(true)
  }

  private fun handleUpdateUser(arguments: Map<String, Any>, result: Result) {
    val customerAttributes = CustomerAttributes()
    (arguments[ARGUMENT_EMAIL] as? String)?.let {
      customerAttributes.setEmail(it)
    }
    (arguments[ARGUMENT_PHONE] as? String)?.let {
      customerAttributes.setPhoneNumber(it)
    }
    (arguments[ARGUMENT_NATIONAL_ID] as? String)?.let {
      customerAttributes.setNationalId(it)
    }
    (arguments[ARGUMENT_FIRST_NAME] as? String)?.let {
      customerAttributes.setFirstName(it)
    }
    (arguments[ARGUMENT_LAST_NAME] as? String)?.let {
      customerAttributes.setLastName(it)
    }
    parseLong(arguments[ARGUMENT_DATE_OF_BIRTH])?.let {
      val date = Date(Timestamp(it).time)
      customerAttributes.setDateOfBirth(date)
    }
    (arguments[ARGUMENT_GENDER_INDEX] as? Int)?.let {
      when (it) {
        1 -> customerAttributes.setGender(Gender.MALE)
        2 -> customerAttributes.setGender(Gender.FEMALE)
        3 -> customerAttributes.setGender(Gender.NONBINARY)
        4 -> customerAttributes.setGender(Gender.UNKNOWN)
        else -> { }
      }
    }
    (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Int>)?.forEach {
      val date = Date()
      var value = parseLong(it.value)
      if (value != null) {
        val date = Date(Timestamp(value).time)
      }
      customerAttributes.put(it.key, date)
    }

    (arguments[ARGUMENT_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        parseCustomerAttributes(it.key, it.value, customerAttributes)
    }
   appConnectInstance.updateCustomer(customerAttributes)
    result.success(true)
  }

  private fun handleClearUser(result: Result) {
    appConnectInstance.clearCustomer()
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleSetSuperAttribute(arguments: Map<String, Any>,  result: Result) {

    val key = arguments[ARGUMENT_KEY] as? String
    if (key.isNullOrEmpty()) {
      throw MissingArgumentException()
    }

    arguments[ARGUMENT_DATE_ATTRIBUTES]?.let {
      var value = parseLong(it)

      if (value != null) {
        val date = Date(Timestamp(value).time)
        appConnectInstance.setSuperAttribute(key, date)
      }
    }

    arguments[ARGUMENT_ATTRIBUTES]?.let {
      when(it) {
        is Int -> appConnectInstance.setSuperAttribute(key, it)
        is Double -> appConnectInstance.setSuperAttribute(key, it)
        is Float -> appConnectInstance.setSuperAttribute(key, it)
        is Boolean -> appConnectInstance.setSuperAttribute(key, it)
        is String -> appConnectInstance.setSuperAttribute(key, it)
        else -> {}
      }
    }

    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleClearSuperAttribute(arguments: Map<String, Any>,  result: Result) {

    val key = arguments[ARGUMENT_KEY] as? String
    if (key.isNullOrEmpty()) {
      throw MissingArgumentException()
    }
    appConnectInstance.clearSuperAttribute(key)

    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleCollectAPMHTTPRecord(arguments: Map<String, Any>, result: Result) {
    val url = arguments[ARGUMENT_URL] as? String
    val statusCode = arguments[ARGUMENT_STATUS_CODE] as? Int
    val duration = arguments[ARGUMENT_DURATION] as? Int
    val success = arguments[ARGUMENT_SUCCESS] as? Boolean
    val methodString = arguments[ARGUMENT_METHOD] as? String

    val method = convertToHttpMethod(methodString)

    if (url.isNullOrEmpty() || method.isNullOrEmpty() || statusCode == null ||
            duration == null || success == null) {
      throw MissingArgumentException()
    }

    val httpCallAttributes = HttpCallAttributes(url, method, statusCode, duration.toLong(), success)

    (arguments[ARGUMENT_REQUEST_SIZE] as? Double)?.let {
      httpCallAttributes.setRequestPayloadSize(it.toLong())
    }
    (arguments[ARGUMENT_RESPONSE_SIZE] as? Double)?.let {
      httpCallAttributes.setResponsePayloadSize(it.toLong())
    }
    (arguments[ARGUMENT_ERROR_TYPE] as? String)?.let {
      httpCallAttributes.setErrorType(it)
    }
    (arguments[ARGUMENT_ERROR_CODE] as? String)?.let {
      httpCallAttributes.setErrorCode(it)
    }
    (arguments[ARGUMENT_ERROR_MESSAGE] as? String)?.let {
      httpCallAttributes.setErrorMessage(it)
    }

    (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      var value = parseLong(it.value)

      if (value != null) {
        val date = Date(Timestamp(value).time)
        httpCallAttributes.put(it.key, date)
      }
    }

    (arguments[ARGUMENT_INT_LIST_ATTRIBUTES] as? Map<String, Any?>)?.forEach {
      val typedVal = (it.value as? List<Int>) ?: listOf<Int>()
      var value = IntArray(typedVal.size)
      for (i in typedVal.indices) {
        value[i] = typedVal[i]
      }
      httpCallAttributes.put(it.key, value)

    }

    (arguments[ARGUMENT_STRING_LIST_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      val typedVal = (it.value as? List<String>) ?: listOf<String>()
      var value = Array<String?>(typedVal.size) {null}
      for (i in typedVal.indices) {
        value[i] = typedVal[i]
      }
      httpCallAttributes.put(it.key, value)
    }

    (arguments[ARGUMENT_CUSTOM_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      when(it.value) {
        is Int -> httpCallAttributes.put(it.key, it.value as Int?)
        is Double -> httpCallAttributes.put(it.key, it.value as Double?)
        is Float -> httpCallAttributes.put(it.key, it.value as Float?)
        is Boolean -> httpCallAttributes.put(it.key, it.value as Boolean?)
        is String -> httpCallAttributes.put(it.key, it.value as String?)
        else -> {}
      }
    }
    appConnectInstance.apmClient.collectHttpCallEvent(httpCallAttributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleCollectAPMNetworkRecord(arguments: Map<String, Any>,  result: Result) {
    val url = arguments[ARGUMENT_URL] as? String
    val methodString = arguments[ARGUMENT_METHOD] as? String
    val exception = arguments[ARGUMENT_EXCEPTION] as? String
    val duration = arguments[ARGUMENT_DURATION] as? Int

    var method = convertToHttpMethod(methodString)

    if (url.isNullOrEmpty() || method.isNullOrEmpty() || exception == null || duration == null) {
      throw MissingArgumentException()
    }

    val errorTypeValue = arguments[ARGUMENT_TYPE] as? Int
    val errorType = parseErrorType(errorTypeValue) ?: throw MissingArgumentException()



    val networkErrorAttributes = NetworkErrorAttributes(url, method, duration.toLong(), errorType, exception)

    (arguments[ARGUMENT_MESSAGE] as? String)?.let {
      networkErrorAttributes.setMessage(it)
    }

    (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      var value = parseLong(it.value)

      if (value != null) {
        val date = Date(Timestamp(value).time)
        networkErrorAttributes.put(it.key, date)
      }
    }

    (arguments[ARGUMENT_INT_LIST_ATTRIBUTES] as? Map<String, Any?>)?.forEach {
      val typedVal = (it.value as? List<Int>) ?: listOf<Int>()
      var value = IntArray(typedVal.size)
      for (i in typedVal.indices) {
        value[i] = typedVal[i]
      }
      networkErrorAttributes.put(it.key, value)

    }

    (arguments[ARGUMENT_STRING_LIST_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      val typedVal = (it.value as? List<String>) ?: listOf<String>()
      var value = Array<String?>(typedVal.size) {null}
      for (i in typedVal.indices) {
        value[i] = typedVal[i]
      }
      networkErrorAttributes.put(it.key, value)
    }

    (arguments[ARGUMENT_CUSTOM_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      when(it.value) {
        is Int -> networkErrorAttributes.put(it.key, it.value as Int?)
        is Double -> networkErrorAttributes.put(it.key, it.value as Double?)
        is Float -> networkErrorAttributes.put(it.key, it.value as Float?)
        is Boolean -> networkErrorAttributes.put(it.key, it.value as Boolean?)
        is String -> networkErrorAttributes.put(it.key, it.value as String?)
        else -> {}
      }
    }

    appConnectInstance.apmClient.collectNetworkErrorEvent(networkErrorAttributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleAddToCart(arguments: Map<String, Any>,  result: Result) {
    val productArguments = arguments[ARGUMENT_PRODUCT] as? Map<String, Any> ?: throw
    MissingArgumentException()
    val product = parseProduct(productArguments)
    val attributes = CartAttributes(product)
    (arguments[ARGUMENT_VALUE] as? Int)?.let {
      attributes.putValue(BigDecimal(it))
    }
    (arguments[ARGUMENT_TOTAL_CART_VALUE] as? Int)?.let {
      attributes.putTotalCartValue(BigDecimal(it))
    }

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectAddToCartEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleAddToWishList(arguments: Map<String, Any>,  result: Result) {
    val productArguments = arguments[ARGUMENT_PRODUCT] as? Map<String, Any> ?: throw
    MissingArgumentException()
    val product = parseProduct(productArguments)
    val attributes = WishListAttributes(product)

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectAddToWishListEvent(attributes)
    result.success(true)
  }

  private fun handleClearCart(arguments: Map<String, Any>,  result: Result) {

    val attributes = ClearCartAttributes()
    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectClearCartEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handlePurchase(arguments: Map<String, Any>,  result: Result) {
    val productsList = arguments[ARGUMENT_PRODUCTS] as? List<Map<String, Any>> ?: throw
    MissingArgumentException()
    val products = productsList.map { parseProduct(it) }

    val currency = arguments[ARGUMENT_CURRENCY] as? String
    val value = arguments[ARGUMENT_VALUE] as? Double
    val success = arguments[ARGUMENT_SUCCESS] as? Boolean
    if (currency.isNullOrEmpty() || value == null || success == null) {
      throw MissingArgumentException()
    }

    val purchaseAttributes = PurchaseAttributes(currency, BigDecimal(value), success).apply {
      putProductList(products)
      (arguments[ARGUMENT_TAX] as? Double)?.let {
        putTax(BigDecimal((it)))
      }
      (arguments[ARGUMENT_SHIP] as? Double)?.let {
        putShip(BigDecimal(it))
      }
      (arguments[ARGUMENT_DISCOUNT] as? Double)?.let {
        putDiscount(BigDecimal((it)))
      }
      (arguments[ARGUMENT_COUPON] as? String)?.let {
        putCoupon(it)
      }
      (arguments[ARGUMENT_TRX_ID] as? String)?.let {
        putTransactionId(it)
      }
      (arguments[ARGUMENT_PAYMENT_METHOD] as? String)?.let {
        putPaymentMethod(it)
      }
      (arguments[ARGUMENT_QUANTITY] as? Int)?.let {
        putQuantity(it)
      }
      (arguments[ARGUMENT_ERROR_CODE] as? String)?.let {
        putErrorCode(it)
      }
      (arguments[ARGUMENT_ERROR_MESSAGE] as? String)?.let {
        putErrorMessage(it)
      }
    }

    putCustomAttributes(arguments, purchaseAttributes)

    appConnectInstance.collectPurchaseEvent(purchaseAttributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleRemoveFromCart(arguments: Map<String, Any>,  result: Result) {
    val productArguments = arguments[ARGUMENT_PRODUCT] as? Map<String, Any> ?: throw
    MissingArgumentException()
    val product = parseProduct(productArguments)
    val attributes = CartAttributes(product)
    (arguments[ARGUMENT_VALUE] as? Int)?.let {
      attributes.putValue(BigDecimal(it))
    }
    (arguments[ARGUMENT_TOTAL_CART_VALUE] as? Int)?.let {
      attributes.putTotalCartValue(BigDecimal(it))
    }

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectRemoveFromCartEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleSearch(arguments: Map<String, Any>,  result: Result) {
    val query = arguments[ARGUMENT_QUERY] as? String ?: throw MissingArgumentException()
    val attributes = SearchAttributes(query)

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectSearchEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleStartCheckout(arguments: Map<String, Any>,  result: Result) {
    val value = arguments[ARGUMENT_VALUE] as? Int
    val currency = arguments[ARGUMENT_CURRENCY] as? String
    if (currency.isNullOrEmpty() || value == null) {
      throw MissingArgumentException()
    }

    val attributes = StartCheckoutAttributes(BigDecimal(value), currency).apply {
      (arguments[ARGUMENT_QUANTITY] as? Int)?.let {
        putQuantity(it)
      }
    }

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectStartCheckoutEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleRemoveFromWishList(arguments: Map<String, Any>,  result: Result) {
    val productArguments = arguments[ARGUMENT_PRODUCT] as? Map<String, Any> ?: throw
    MissingArgumentException()
    val product = parseProduct(productArguments)
    val attributes = WishListAttributes(product)

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectRemoveFromWishListEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleViewCategory(arguments: Map<String, Any>,  result: Result) {
    val category = arguments[ARGUMENT_CATEGORY] as? String ?: throw MissingArgumentException()
    val attributes = ViewCategoryAttributes(category)

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectViewCategoryEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleViewProduct(arguments: Map<String, Any>,  result: Result) {
    val productArguments = arguments[ARGUMENT_PRODUCT] as? Map<String, Any> ?: throw
    MissingArgumentException()
    val product = parseProduct(productArguments)
    val attributes = ViewProductAttributes(product)

    putCustomAttributes(arguments, attributes)

    appConnectInstance.collectViewProductEvent(attributes)
    result.success(true)
  }

  private fun handleEnableGeofencing( result: Result) {
    appConnectInstance.enableGeoFencing()
    result.success(true)
  }

  private fun handleDisableGeofencing( result: Result) {
    appConnectInstance.disableGeoFencing()
    result.success(true)
  }

  private fun handleUpdateLanguage(arguments: Map<String, Any>,  result: Result) {
    (arguments[ARGUMENT_LANGUAGE_CODE] as? String)?.let {
      appConnectInstance.updateLanguage(Locale(it))
    }
    result.success(true)
  }

  private fun handleEnablePush( result: Result) {
    //TODO: Get callback functions
    appConnectInstance.enablePush { result ->
      when (result) {
        PushRegistrationResult.RESULT_CODE_FAILED -> {}
        PushRegistrationResult.RESULT_CODE_OK -> {}
        PushRegistrationResult.RESULT_CODE_PENDING -> {}
      }
    }
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleStartTracking(arguments: Map<String, Any>,  result: Result) {
    val viewClass = (arguments[ARGUMENT_VIEW_CLASS] as? String) ?: throw MissingArgumentException()
    val label = (arguments[ARGUMENT_LABEL] as? String) ?: throw MissingArgumentException()

    var screenTrackingAttributes = ScreenTrackingAttributes(viewClass, label).apply {
      (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        var value = parseLong(it.value)

        if (value != null) {
          val date = Date(Timestamp(value).time)
          put(it.key, date)
        }
      }

      (arguments[ARGUMENT_INT_LIST_ATTRIBUTES] as? Map<String, Any?>)?.forEach {
        val typedVal = (it.value as? List<Int>) ?: listOf<Int>()
        var value = IntArray(typedVal.size)
        for (i in typedVal.indices) {
          value[i] = typedVal[i]
        }
        put(it.key, value)

      }

      (arguments[ARGUMENT_STRING_LIST_ATTRIBUTES] as? Map<String, Any>)?.forEach {

        val typedVal = (it.value as? List<String>) ?: listOf<String>()
        var value = Array<String?>(typedVal.size) {null}
        for (i in typedVal.indices) {
          value[i] = typedVal[i]
        }
        put(it.key, value)
      }

      (arguments[ARGUMENT_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        when(it.value) {
          is Int -> put(it.key, it.value as Int?)
          is Double -> put(it.key, it.value as Double?)
          is Float -> put(it.key, it.value as Float?)
          is Boolean -> put(it.key, it.value as Boolean?)
          is String -> put(it.key, it.value as String?)
          else -> {}
        }
      }
    }
    appConnectInstance.screenTracker.viewStarted(screenTrackingAttributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleStopTracking(arguments: Map<String, Any>,  result: Result) {
    val viewClass = (arguments[ARGUMENT_VIEW_CLASS] as? String) ?: throw MissingArgumentException()
    val label = (arguments[ARGUMENT_LABEL] as? String) ?: throw MissingArgumentException()

    var screenTrackingAttributes = ScreenTrackingAttributes(viewClass, label)

    appConnectInstance.screenTracker.viewStopped(screenTrackingAttributes)
    result.success(true)
  }

  private fun handleFetchInboxMessages(arguments: Map<String, Any>,  result: Result) {
    val queryJSON = arguments[ARGUMENT_QUERY] as? Map<String, Any>
    var inboxQuery: InboxQuery? = null
    queryJSON?.let { json ->
      val queryBuilder = InboxQuery.Builder()
      (json[ARGUMENT_MESSAGE_TYPE] as? Int)?.let {
        when (it) {
          0 -> queryBuilder.type(InboxMessageType.PUSH_MESSAGE)
          1 -> queryBuilder.type(InboxMessageType.INAPP_MESSAGE)
          else -> print("")
        }
      }
      (json[ARGUMENT_MESSAGE_STATUS] as? Int)?.let {
        when (it) {
          0 -> queryBuilder.status(InboxMessageStatus.UNREAD)
          1 -> queryBuilder.status(InboxMessageStatus.READ)
          2 -> queryBuilder.status(InboxMessageStatus.DISMISSED)
          else -> print("")
        }
      }
      (json[ARGUMENT_IS_ANONYMOUS] as? Boolean)?.let {
        queryBuilder.anonymous(it)
      }
      (json[ARGUMENT_FROM] as? Long)?.let {
        val date = Date(Timestamp(it).time)
        queryBuilder.from(date)
      }
      (json[ARGUMENT_TO] as? Long)?.let {
        val date = Date(Timestamp(it).time)
        queryBuilder.to(date)
      }
      inboxQuery = queryBuilder.build()
    }

    fun onGetCallback(messages: List<InboxMessage>): List<Map<String, Any>> {
      val messageList = messages.map {
        var messageTypeIndex = -1;
        when (it.type) {
          InboxMessageType.PUSH_MESSAGE -> messageTypeIndex = 0
          InboxMessageType.INAPP_MESSAGE -> messageTypeIndex = 1
        }
        var messageStatusIndex = 0;
        when (it.type) {
          InboxMessageStatus.UNREAD -> messageStatusIndex = 0
          InboxMessageStatus.READ -> messageStatusIndex = 1
          InboxMessageStatus.DISMISSED -> messageStatusIndex = 2
        }
        var m = mutableMapOf<String, Any>(
                ARGUMENT_ID to it.id.toString(),
                ARGUMENT_MESSAGE_TYPE to messageTypeIndex,
                ARGUMENT_MESSAGE_STATUS to messageStatusIndex
        )
        it.expirationDate?.let { date ->
          m[ARGUMENT_EXPIRATION_DATE] = date.time.toDouble()
        }
        it.receivedDate?.let { date ->
          m[ARGUMENT_RECEIVED_DATE] = date.time.toDouble()
        }
        it.customerId?.let { customerId ->
          m[ARGUMENT_CUSTOMER_ID] = customerId
        }
        it.payload?.let { payload ->
          m[ARGUMENT_PAYLOAD] = payload
        }
        m.toMap()
      }
      return messageList
    }

    if (inboxQuery == null) {
      appConnectInstance.getInboxClient().getMessages {
        onGetCallback(it)
        result.success(JSONArray(onGetCallback(it)).toString())
      }
    } else {
      appConnectInstance.getInboxClient().getMessages(inboxQuery!!) {
        onGetCallback(it)
        result.success(JSONArray(onGetCallback(it)).toString())
      }
    }
  }

  private fun handleDeleteInboxMessages(arguments: Map<String, Any>,  result: Result) {
    val messageIDList = (arguments[ARGUMENT_MESSAGE_ID_LIST] as? List<String>) ?: emptyList()
    appConnectInstance.getInboxClient().deleteMessages(messageIDList.map { it.toLong() }, null)
    result.success(true)
  }

  private fun handleReadInboxMessages(arguments: Map<String, Any>,  result: Result) {
    val messageIDList = (arguments[ARGUMENT_MESSAGE_ID_LIST] as? List<String>) ?: emptyList()
    appConnectInstance.getInboxClient().readMessage(messageIDList.map { it.toLong() }, null)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleCollectDeeplink(arguments: Map<String, Any>,  result: Result) {
    val url = (arguments[ARGUMENT_URL] as? String) ?: throw MissingArgumentException()

    val attributes: DeeplinkLaunchedAttributes = DeeplinkLaunchedAttributes(Uri.parse(url)).apply {
      (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        var value = parseLong(it.value)

        if (value != null) {
          val date = Date(Timestamp(value).time)
          put(it.key, date)
        }
      }

      (arguments[ARGUMENT_INT_LIST_ATTRIBUTES] as? Map<String, Any?>)?.forEach {
        val typedVal = (it.value as? List<Int>) ?: listOf<Int>()
        var value = IntArray(typedVal.size)
        for (i in typedVal.indices) {
          value[i] = typedVal[i]
        }
        put(it.key, value)

      }

      (arguments[ARGUMENT_STRING_LIST_ATTRIBUTES] as? Map<String, Any>)?.forEach {

        val typedVal = (it.value as? List<String>) ?: listOf<String>()
        var value = Array<String?>(typedVal.size) {null}
        for (i in typedVal.indices) {
          value[i] = typedVal[i]
        }
        put(it.key, value)
      }

      (arguments[ARGUMENT_ATTRIBUTES] as? Map<String, Any>)?.forEach {
        when(it.value) {
          is Int -> put(it.key, it.value as Int?)
          is Double -> put(it.key, it.value as Double?)
          is Float -> put(it.key, it.value as Float?)
          is Boolean -> put(it.key, it.value as Boolean?)
          is String -> put(it.key, it.value as String?)
          else -> {}
        }
      }
    }

    appConnectInstance.collectDeeplinkLaunchedEvent(attributes)
    result.success(true)
  }

  @Throws(MissingArgumentException::class)
  private fun handleCollectButtonClickEvent(arguments: Map<String, Any>,  result: Result) {

    val buttonClickAttributes = ButtonClickAttributes().apply {

      (arguments[ARGUMENT_LABEL] as? String)?.let {
        setLabel(it)
      }

      (arguments[ARGUMENT_ACCESSIBILITY_LABEL] as? String)?.let {
        setAccessibilityLabel(it)
      }

      (arguments[ARGUMENT_COMPONENT_ID] as? String)?.let {
        setComponentId(it)
      }

      (arguments[ARGUMENT_CLASS_NAME] as? String)?.let {
        setClassName(it)
      }

      (arguments[ARGUMENT_COORDINATES] as? Map<String, Any>)?.let {coordinates ->
        val left = (coordinates[ARGUMENT_LEFT]) as? Int ?: throw MissingArgumentException()
        val top = (coordinates[ARGUMENT_TOP]) as? Int ?: throw MissingArgumentException()
        val right = (coordinates[ARGUMENT_RIGHT]) as? Int ?: throw MissingArgumentException()
        val bottom = (coordinates[ARGUMENT_BOTTOM]) as? Int ?: throw MissingArgumentException()

        setCoordinates(Coordinates(left, top, right, bottom))
      }

      (arguments[ARGUMENT_SCREEN_TRACKING_ATTRIBUTES] as? Map<String, Any>)?.let { attributes ->
        val viewClass = (attributes[ARGUMENT_VIEW_CLASS]) as? String ?: throw MissingArgumentException()
        val viewLabel = (attributes[ARGUMENT_LABEL]) as? String ?: throw MissingArgumentException()
        setScreenTrackingAttributes(ScreenTrackingAttributes(viewClass, viewLabel))
      }
    }

    appConnectInstance.autoCaptureClient.collectButtonClickEvent(buttonClickAttributes)

    result.success(true)
  }
  @Throws(MissingArgumentException::class)
  private fun handleCollectTextChangeEvent(arguments: Map<String, Any>,  result: Result) {

    val textChangeAttributes = TextChangeAttributes().apply {

      (arguments[ARGUMENT_PLACEHOLDER] as? String)?.let {
        setPlaceholder(it)
      }

      (arguments[ARGUMENT_VALUE] as? String)?.let {
        setTextValue(it)
      }

      (arguments[ARGUMENT_ACCESSIBILITY_LABEL] as? String)?.let {
        setAccessibilityLabel(it)
      }

      (arguments[ARGUMENT_COMPONENT_ID] as? String)?.let {
        setComponentId(it)
      }

      (arguments[ARGUMENT_CLASS_NAME] as? String)?.let {
        setClassName(it)
      }

      (arguments[ARGUMENT_COORDINATES] as? Map<String, Any>)?.let {coordinates ->
        val left = (coordinates[ARGUMENT_LEFT]) as? Int ?: throw MissingArgumentException()
        val top = (coordinates[ARGUMENT_TOP]) as? Int ?: throw MissingArgumentException()
        val right = (coordinates[ARGUMENT_RIGHT]) as? Int ?: throw MissingArgumentException()
        val bottom = (coordinates[ARGUMENT_BOTTOM]) as? Int ?: throw MissingArgumentException()

        setCoordinates(Coordinates(left, top, right, bottom))
      }

      (arguments[ARGUMENT_SCREEN_TRACKING_ATTRIBUTES] as? Map<String, Any>)?.let { attributes ->
        val viewClass = (attributes[ARGUMENT_VIEW_CLASS]) as? String ?: throw MissingArgumentException()
        val viewLabel = (attributes[ARGUMENT_LABEL]) as? String ?: throw MissingArgumentException()
        setScreenTrackingAttributes(ScreenTrackingAttributes(viewClass, viewLabel))
      }
    }

    appConnectInstance.autoCaptureClient.collectTextChangeEvent(textChangeAttributes)

    result.success(true)
  }
  @Throws(MissingArgumentException::class)
  private fun handleCollectToggleChangeEvent(arguments: Map<String, Any>,  result: Result) {

    val toggleChangeAttributes = ToggleChangeAttributes().apply {

      (arguments[ARGUMENT_LABEL] as? String)?.let {
        setLabel(it)
      }

      (arguments[ARGUMENT_VALUE] as? Boolean)?.let {
        setIsChecked(it)
      }

      (arguments[ARGUMENT_ACCESSIBILITY_LABEL] as? String)?.let {
        setAccessibilityLabel(it)
      }

      (arguments[ARGUMENT_COMPONENT_ID] as? String)?.let {
        setComponentId(it)
      }

      (arguments[ARGUMENT_CLASS_NAME] as? String)?.let {
        setClassName(it)
      }

      (arguments[ARGUMENT_COORDINATES] as? Map<String, Any>)?.let {coordinates ->
        val left = (coordinates[ARGUMENT_LEFT]) as? Int ?: throw MissingArgumentException()
        val top = (coordinates[ARGUMENT_TOP]) as? Int ?: throw MissingArgumentException()
        val right = (coordinates[ARGUMENT_RIGHT]) as? Int ?: throw MissingArgumentException()
        val bottom = (coordinates[ARGUMENT_BOTTOM]) as? Int ?: throw MissingArgumentException()

        setCoordinates(Coordinates(left, top, right, bottom))
      }

      (arguments[ARGUMENT_SCREEN_TRACKING_ATTRIBUTES] as? Map<String, Any>)?.let { attributes ->
        val viewClass = (attributes[ARGUMENT_VIEW_CLASS]) as? String ?: throw MissingArgumentException()
        val viewLabel = (attributes[ARGUMENT_LABEL]) as? String ?: throw MissingArgumentException()
        setScreenTrackingAttributes(ScreenTrackingAttributes(viewClass, viewLabel))
      }
    }

    appConnectInstance.autoCaptureClient.collectToggleChangeEvent(toggleChangeAttributes)

    result.success(true)
  }
  @Throws(MissingArgumentException::class)
  private fun handleCollectTouchEvent (arguments: Map<String, Any>,  result: Result) {

    val touchAttributes = TouchAttributes().apply {

      (arguments[ARGUMENT_ACCESSIBILITY_LABEL] as? String)?.let {
        setAccessibilityLabel(it)
      }

      (arguments[ARGUMENT_COMPONENT_ID] as? String)?.let {
        setComponentId(it)
      }

      (arguments[ARGUMENT_CLASS_NAME] as? String)?.let {
        setClassName(it)
      }

      (arguments[ARGUMENT_COORDINATES] as? Map<String, Any>)?.let {coordinates ->
        val left = (coordinates[ARGUMENT_LEFT]) as? Int ?: throw MissingArgumentException()
        val top = (coordinates[ARGUMENT_TOP]) as? Int ?: throw MissingArgumentException()
        val right = (coordinates[ARGUMENT_RIGHT]) as? Int ?: throw MissingArgumentException()
        val bottom = (coordinates[ARGUMENT_BOTTOM]) as? Int ?: throw MissingArgumentException()

        setCoordinates(Coordinates(left, top, right, bottom))
      }

      (arguments[ARGUMENT_SCREEN_TRACKING_ATTRIBUTES] as? Map<String, Any>)?.let { attributes ->
        val viewClass = (attributes[ARGUMENT_VIEW_CLASS]) as? String ?: throw MissingArgumentException()
        val viewLabel = (attributes[ARGUMENT_LABEL]) as? String ?: throw MissingArgumentException()
        setScreenTrackingAttributes(ScreenTrackingAttributes(viewClass, viewLabel))
      }
      (arguments[ARGUMENT_TOUCH_POINT] as? Map<String, Any>)?.let { attributes ->
        val x = (attributes[ARGUMENT_X]) as? Int ?: throw MissingArgumentException()
        val y = (attributes[ARGUMENT_Y]) as? Int ?: throw MissingArgumentException()
        setTouchPoint(TouchPoint(x,y))
      }
    }

    appConnectInstance.autoCaptureClient.collectTouchEvent(touchAttributes)

    result.success(true)
  }
  @Throws(MissingArgumentException::class)
  private fun handleCollectDoubleTapEvent (arguments: Map<String, Any>,  result: Result) {

    val doubleTapAttributes = DoubleTapAttributes().apply {

      (arguments[ARGUMENT_ACCESSIBILITY_LABEL] as? String)?.let {
        setAccessibilityLabel(it)
      }

      (arguments[ARGUMENT_COMPONENT_ID] as? String)?.let {
        setComponentId(it)
      }

      (arguments[ARGUMENT_CLASS_NAME] as? String)?.let {
        setClassName(it)
      }

      (arguments[ARGUMENT_COORDINATES] as? Map<String, Any>)?.let {coordinates ->
        val left = (coordinates[ARGUMENT_LEFT]) as? Int ?: throw MissingArgumentException()
        val top = (coordinates[ARGUMENT_TOP]) as? Int ?: throw MissingArgumentException()
        val right = (coordinates[ARGUMENT_RIGHT]) as? Int ?: throw MissingArgumentException()
        val bottom = (coordinates[ARGUMENT_BOTTOM]) as? Int ?: throw MissingArgumentException()

        setCoordinates(Coordinates(left, top, right, bottom))
      }

      (arguments[ARGUMENT_SCREEN_TRACKING_ATTRIBUTES] as? Map<String, Any>)?.let { attributes ->
        val viewClass = (attributes[ARGUMENT_VIEW_CLASS]) as? String ?: throw MissingArgumentException()
        val viewLabel = (attributes[ARGUMENT_LABEL]) as? String ?: throw MissingArgumentException()
        setScreenTrackingAttributes(ScreenTrackingAttributes(viewClass, viewLabel))
      }
      (arguments[ARGUMENT_TOUCH_POINT] as? Map<String, Any>)?.let { attributes ->
        val x = (attributes[ARGUMENT_X]) as? Int ?: throw MissingArgumentException()
        val y = (attributes[ARGUMENT_Y]) as? Int ?: throw MissingArgumentException()
        setTouchPoint(TouchPoint(x,y))
      }
    }

    appConnectInstance.autoCaptureClient.collectDoubleTapEvent(doubleTapAttributes)

    result.success(true)
  }
  @Throws(MissingArgumentException::class)
  private fun handleCollectLongPressEvent (arguments: Map<String, Any>,  result: Result) {

    val longPressAttributes = LongPressAttributes().apply {

      (arguments[ARGUMENT_ACCESSIBILITY_LABEL] as? String)?.let {
        setAccessibilityLabel(it)
      }

      (arguments[ARGUMENT_COMPONENT_ID] as? String)?.let {
        setComponentId(it)
      }

      (arguments[ARGUMENT_CLASS_NAME] as? String)?.let {
        setClassName(it)
      }

      (arguments[ARGUMENT_COORDINATES] as? Map<String, Any>)?.let {coordinates ->
        val left = (coordinates[ARGUMENT_LEFT]) as? Int ?: throw MissingArgumentException()
        val top = (coordinates[ARGUMENT_TOP]) as? Int ?: throw MissingArgumentException()
        val right = (coordinates[ARGUMENT_RIGHT]) as? Int ?: throw MissingArgumentException()
        val bottom = (coordinates[ARGUMENT_BOTTOM]) as? Int ?: throw MissingArgumentException()

        setCoordinates(Coordinates(left, top, right, bottom))
      }

      (arguments[ARGUMENT_SCREEN_TRACKING_ATTRIBUTES] as? Map<String, Any>)?.let { attributes ->
        val viewClass = (attributes[ARGUMENT_VIEW_CLASS]) as? String ?: throw MissingArgumentException()
        val viewLabel = (attributes[ARGUMENT_LABEL]) as? String ?: throw MissingArgumentException()
        setScreenTrackingAttributes(ScreenTrackingAttributes(viewClass, viewLabel))
      }
      (arguments[ARGUMENT_TOUCH_POINT] as? Map<String, Any>)?.let { attributes ->
        val x = (attributes[ARGUMENT_X]) as? Int ?: throw MissingArgumentException()
        val y = (attributes[ARGUMENT_Y]) as? Int ?: throw MissingArgumentException()
        setTouchPoint(TouchPoint(x,y))
      }
    }

    appConnectInstance.autoCaptureClient.collectLongPressEvent(longPressAttributes)

    result.success(true)
  }
  @Throws(MissingArgumentException::class)
  private fun handleCollectSwipeEvent (arguments: Map<String, Any>,  result: Result) {

    val swipeAttributes = SwipeAttributes().apply {

      (arguments[ARGUMENT_ACCESSIBILITY_LABEL] as? String)?.let {
        setAccessibilityLabel(it)
      }

      (arguments[ARGUMENT_COMPONENT_ID] as? String)?.let {
        setComponentId(it)
      }

      (arguments[ARGUMENT_CLASS_NAME] as? String)?.let {
        setClassName(it)
      }

      (arguments[ARGUMENT_COORDINATES] as? Map<String, Any>)?.let {coordinates ->
        val left = (coordinates[ARGUMENT_LEFT]) as? Int ?: throw MissingArgumentException()
        val top = (coordinates[ARGUMENT_TOP]) as? Int ?: throw MissingArgumentException()
        val right = (coordinates[ARGUMENT_RIGHT]) as? Int ?: throw MissingArgumentException()
        val bottom = (coordinates[ARGUMENT_BOTTOM]) as? Int ?: throw MissingArgumentException()

        setCoordinates(Coordinates(left, top, right, bottom))
      }

      (arguments[ARGUMENT_SCREEN_TRACKING_ATTRIBUTES] as? Map<String, Any>)?.let { attributes ->
        val viewClass = (attributes[ARGUMENT_VIEW_CLASS]) as? String ?: throw MissingArgumentException()
        val viewLabel = (attributes[ARGUMENT_LABEL]) as? String ?: throw MissingArgumentException()
        setScreenTrackingAttributes(ScreenTrackingAttributes(viewClass, viewLabel))
      }
      (arguments[ARGUMENT_SWIPE_POINTS] as? Map<String, Any>)?.let { attributes ->
        val start = (attributes[ARGUMENT_START]) as? Map<String, Any> ?: throw MissingArgumentException()
        val end = (attributes[ARGUMENT_END]) as? Map<String, Any> ?: throw MissingArgumentException()

        val startX = (start[ARGUMENT_X]) as? Int ?: throw MissingArgumentException()
        val startY = (start[ARGUMENT_Y]) as? Int ?: throw MissingArgumentException()

        val endX = (end[ARGUMENT_X]) as? Int ?: throw MissingArgumentException()
        val endY = (end[ARGUMENT_Y]) as? Int ?: throw MissingArgumentException()

        setSwipePoints(TouchPoint(startX, startY), TouchPoint(endX, endY))
      }
    }

    appConnectInstance.autoCaptureClient.collectSwipeEvent(swipeAttributes)

    result.success(true)
  }

  @SuppressLint("RestrictedApi")
  @Throws(MissingArgumentException::class)
  private fun handlePushMessageReceived(arguments: Map<String, Any>, result: Result) {
      val logger = ConnectCommonLog.getInstance()
      try {
          if (AppConnectInstanceRegistry.getRegistry().all.isEmpty()) {
              logger.error("Received push notification but no AppConnect instance found, skipping.")
              return
          }
          val args = arguments as Map<String, String>
          val isDataroid = RemoteNotificationHandler(
              AppConnectPushNotificationConverter(AppConnectJsonConverter.getInstance(), logger),
              SystemCurrentTimeProvider.newInstance(),
              logger
          ).handle(args)
          result.success(isDataroid)
      } catch (e: Exception) {
          logger.error("Error invoking AppConnectFCMReceiverHelper", e)
      }
  }

  private fun handleUpdateConfig(arguments: Map<String, Any>,  result: Result) {
    (arguments[ARGUMENT_SESSION_DROP_DURATION] as? Double)?.let {
      appConnectInstance.config.sessionDropDuration = it.toInt() * 1000 // convert to milliseconds
    }

    (arguments[ARGUMENT_INBOX] as? Map<String, Any>)?.let {
      (it[ARGUMENT_IS_ENABLED] as? Boolean)?.let { isEnabled ->
        appConnectInstance.config.appConnectInboxConfig.isEnabled = isEnabled
      }
      (it[ARGUMENT_STORAGE_LIMIT] as? Int)?.let { limit ->
        appConnectInstance.config.appConnectInboxConfig.storageLimit = limit
      }
    }

    (arguments[ARGUMENT_SNAPSHOT] as? Map<String, Any>)?.let {
        (it[ARGUMENT_RECORDING_ENABLED] as? Boolean)?.let { isEnabled ->
          appConnectInstance.config.appConnectSnapshotConfig.isEnabled = isEnabled
          // TODO: Allowed package name updates
        }
    }

    (arguments[ARGUMENT_IN_APP_MESSAGING] as? Map<String, Any>)?.let {
      (it[ARGUMENT_IN_APP_MESSAGING_ENABLED] as? Boolean)?.let { isEnabled ->
        appConnectInstance.config.inAppMessagingConfig.isEnabled = isEnabled
      }
    }

    (arguments[ARGUMENT_APM] as? Map<String, Any>)?.let {
      (it[ARGUMENT_RECORD_COLLECTION_ENABLED] as? Boolean)?.let { isEnabled ->
        appConnectInstance.config.apmConfig.isEnabled = isEnabled
      }
      (it[ARGUMENT_RECORD_STORAGE_LIMIT] as? Int)?.let { storageLimit ->
        appConnectInstance.config.apmConfig.storageLimit = storageLimit
      }
    }

    (arguments[ARGUMENT_SCREEN_TRACKING] as? Map<String, Any>)?.let {
      (it[ARGUMENT_ENABLED] as? Boolean)?.let { isEnabled ->
        appConnectInstance.config.screenTrackingConfig.setDefaultActivityTrackingEnabled(isEnabled)
      }
    }

    (arguments[ARGUMENT_NOTIFICATION_CONFIG] as? Map<String, Any>)?.let {
      (it[ARGUMENT_SMALL_NOTIFICATION_ICON] as? Int)?.let { icon ->
        appConnectInstance.config.notificationConfig.smallNotificationIcon = icon
      }
      (it[ARGUMENT_LARGE_NOTIFICATION_ICON] as? Int)?.let { icon ->
        appConnectInstance.config.notificationConfig.largeNotificationIcon = icon
      }
      (it[ARGUMENT_DEFAULT_NOTIFICATION_CHANNEL_ID] as? String)?.let { channelId ->
        appConnectInstance.config.notificationConfig.defaultNotificationChannelId = channelId
      }
      (it[ARGUMENT_DEFAULT_NOTIFICATION_CHANNEL_NAME] as? String)?.let { channelName ->
        appConnectInstance.config.notificationConfig.defaultNotificationChannelName = channelName
      }
    }

    result.success(true)
  }

  private fun parseLong(value: Any?): Long? {
    if (value == null) {
      return null
    }
    val longValue = value as? Long
    val intValue = (value as? Int)?.toLong()
    return longValue ?: intValue
  }

  private fun parseErrorType(value: Int?): String? {
    if (value == null) {
      return null
    }
    when (value) {
      0 -> return NetworkErrorErrorType.UNKNOWN
      1 -> return NetworkErrorErrorType.NO_CONNECTION_ERROR
      2 -> return NetworkErrorErrorType.SSL_ERROR
      4 -> return NetworkErrorErrorType.TIMEOUT_ERROR
      8 -> return NetworkErrorErrorType.AUTH_FAILURE_ERROR
      16 -> return NetworkErrorErrorType.NETWORK_ERROR
      32 -> return NetworkErrorErrorType.PARSE_ERROR
      64 -> return NetworkErrorErrorType.SERVER_ERROR
      128 -> return NetworkErrorErrorType.CANCELLED_ERROR
    }
    return null
  }

  @Throws(MissingArgumentException::class)
  private fun parseProduct(productArguments: Map<String, Any>): Product {
    val id = productArguments[ARGUMENT_ID] as? String ?: throw MissingArgumentException()
    val name = productArguments[ARGUMENT_NAME] as? String ?: throw MissingArgumentException()
    val quantity = productArguments[ARGUMENT_QUANTITY] as? Int ?: throw MissingArgumentException()
    val price = productArguments[ARGUMENT_PRICE] as? Double ?: throw MissingArgumentException()
    val currency = productArguments[ARGUMENT_CURRENCY] as? String ?: throw MissingArgumentException()
    val productBuilder = Product.Builder.newInstance(id, name, quantity, BigDecimal(price),
            currency)
            ?: throw MissingArgumentException()
    (productArguments[ARGUMENT_VARIANT] as? String)?.let {
      productBuilder.setVariant(it)
    }
    (productArguments[ARGUMENT_DESCRIPTION] as? String)?.let { it ->
      productBuilder.setProductDescription(it)
    }
    (productArguments[ARGUMENT_BRAND] as? String)?.let {
      productBuilder.setBrand(it)
    }
    (productArguments[ARGUMENT_CATEGORY] as? String)?.let {
      productBuilder.setCategory(it)
    }
    return productBuilder.build()
  }

  private fun putCustomAttributes(arguments: Map<String, Any>, attributes: AttributeBuilder<*>) {
    (arguments[ARGUMENT_DATE_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      var value = parseLong(it.value)

      if (value != null) {
        val date = Date(Timestamp(value).time)
        attributes.put(it.key, date)
      }
    }

    (arguments[ARGUMENT_INT_LIST_ATTRIBUTES] as? Map<String, Any?>)?.forEach {
      val typedVal = (it.value as? List<Int>) ?: listOf<Int>()
      var value = IntArray(typedVal.size)
      for (i in typedVal.indices) {
        value[i] = typedVal[i]
      }
      attributes.put(it.key, value)

    }

    (arguments[ARGUMENT_STRING_LIST_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      val typedVal = (it.value as? List<String>) ?: listOf<String>()
      var value = Array<String?>(typedVal.size) {null}
      for (i in typedVal.indices) {
        value[i] = typedVal[i]
      }
      attributes.put(it.key, value)
    }

    (arguments[ARGUMENT_ATTRIBUTES] as? Map<String, Any>)?.forEach {
      when(it.value) {
        is Int -> attributes.put(it.key, it.value as Int?)
        is Double -> attributes.put(it.key, it.value as Double?)
        is Float -> attributes.put(it.key, it.value as Float?)
        is Boolean -> attributes.put(it.key, it.value as Boolean?)
        is String -> attributes.put(it.key, it.value as String?)
        else -> {}
      }
    }
  }
  companion object {

    lateinit var appConnectInstance: AppConnect
    fun initialize(context: Context, config: DataroidPluginConfig) : AppConnect {
      val builder = AppConnect.Builder(context as Application, config.sdkKey)
              .withUrl(config.serverURL)
              .withFramework("FLUTTER")

      config.apply {
        languageCode?.let { builder.withLanguage(Locale(it)) }
        isEventCollectingDisabled?.let {
          builder.withEventCollectingDisabled(it)
        }
        eventStorageLimit?.let { builder.withEventStorageLimit(it) }


        val logConfig = AppConnectLogConfig("LogTag").also { config ->
          logLevel?.let { config.withLogLevel(it.level) }

          val logDirectory: File? = context
            .getExternalFilesDir(null)

          isFileLoggingEnabled?.let {
            if (logDirectory != null) {
              config.withFileLogging(logDirectory, "AppConnectLog")
            }
          }
        }

        builder.withLogConfig(logConfig)

        val screenTrackingConfig = AppConnectScreenTrackingConfig()
        isScreenTrackingEnabled?.let {
          screenTrackingConfig.setDefaultActivityTrackingEnabled(it)
        }
        builder.withScreenTrackingConfig(screenTrackingConfig)
        isInAppMessagingEnabled?.let {
          val inAppMessagingConfig = AppConnectInAppMessagingConfig()
          inAppMessagingConfig.isEnabled = it
          builder.withInAppMessagingConfig(inAppMessagingConfig)
        }
        isAPMEnabled?.let {
          val apmConfig = AppConnectAPMConfig().apply {
            withEnabled(it)
          }
          builder.withAPMConfig(apmConfig)
        }
        if (!pinningEndpoint.isNullOrEmpty() && !pinningKey.isNullOrEmpty()) {
          val networkConfig = AppConnectNetworkConfig();
          val certificatePinningConfig = AppConnectCertificatePinningConfig()
          certificatePinningConfig.addCertificate(pinningEndpoint!!, pinningKey!!)
          networkConfig.setCertificatePinning(certificatePinningConfig)
          builder.withNetworkConfig(networkConfig)
        }
        sessionDropDuration?.let {
          builder.withSessionDropDuration(it.toInt() * 1000) // convert to milliseconds
        }
      }

      appConnectInstance = builder.build()
      return appConnectInstance
    }

  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    activity = binding.activity
  }

  override fun onDetachedFromActivity() {
    activity = null
  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    activity = binding.activity
  }

  override fun onDetachedFromActivityForConfigChanges() {
    activity = null
  }
}

private class MissingArgumentException(): Exception("Missing arguments")